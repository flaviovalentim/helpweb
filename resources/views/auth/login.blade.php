@extends('layouts.auth')

@section('content')
    {{--<div class="container">--}}
        {{--<div class="col-md-4 col-md-offset-4">--}}
            {{--<div class="info">--}}
                {{--<h1>{{ config('app.name') }}</h1>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    <div class="accountbg"></div>
    <div class="wrapper-login">
        <div class="panel panel-primary panel-login">
            <div class="panel-login-body">
                <h3 class="text-center mtp-0 mbt-15">
                    <a href="index.html" class="login login-admin">
                        <span>H</span>elp
                    </a>
                </h3>
                <form method="POST" action="{{ route('login') }}" class="form-horizontal mtp-0" >
                    {{ csrf_field() }}
                    <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                        <div class="col-xs-12">
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus placeholder="E-mail">
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <div class="col-xs-12">
                            <input id="password" class="form-control" type="password" name="password" required placeholder="Password">
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group text-center m-t-40">
                        <div class="col-xs-12">
                            <button class="btn btn-success btn-block btn-lg waves-effect waves-light" type="submit">Login</button>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <a class="btn btn-link" href="{{ route('password.request') }}">
                                Esqueceu sua senha?
                            </a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{--<div class="container">--}}
        {{--<div class="flex-align">--}}
            {{--<div class="col-md-3 col-md-offset-4-5">--}}
                {{--<div class="form">--}}
                    {{--<div class="preview" style="font-size: 80px!important; color: #521941!important;">--}}
                        {{--<i class="fa fa-ambulance"></i>--}}
                    {{--</div>--}}

                    {{--<form method="POST" action="{{ route('login') }}">--}}
                        {{--{{ csrf_field() }}--}}
                        {{--<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">--}}
                            {{--<label for="email" style="text-align:left;">E-Mail</label>--}}
                            {{--<input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>--}}

                            {{--@if ($errors->has('email'))--}}
                                {{--<span class="help-block">--}}
                                    {{--<strong>{{ $errors->first('email') }}</strong>--}}
                                {{--</span>--}}
                            {{--@endif--}}
                        {{--</div>--}}

                        {{--<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">--}}
                            {{--<label for="password">Senha</label>--}}
                            {{--<input id="password" type="password" class="form-control" name="password" required>--}}

                            {{--@if ($errors->has('password'))--}}
                                {{--<span class="help-block">--}}
                                    {{--<strong>{{ $errors->first('password') }}</strong>--}}
                                {{--</span>--}}
                            {{--@endif--}}

                        {{--</div>--}}
                        {{--<br />--}}
                        {{--<div class="row">--}}
                            {{--<div class="form-group">--}}
                                {{--<div class="col-md-12">--}}
                                    {{--<button type="submit" class="btn btn-success">Login</button>--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="form-group">--}}
                                {{--<div class="col-md-12">--}}
                                    {{--<a class="btn btn-link" href="{{ route('password.request') }}">--}}
                                        {{--Esqueceu sua senha?--}}
                                    {{--</a>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</form>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}

@endsection

