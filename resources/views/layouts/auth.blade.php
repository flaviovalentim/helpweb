<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ config('app.name') }}</title>
    <!-- Bottstrap -->
    <link rel="stylesheet" href="{{ asset('vendor/AdminLTE/bootstrap/css/bootstrap.min.css') }}">
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <style>
        div.wrapper-login {
            margin: 7.5% auto!important;
            width: 420px!important;
            position: relative!important;
        }
        div.panel-login {
            border: 4px solid #eee !important;
        }

        input.form-control {
            -moz-border-radius: 2px;
            -moz-box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.1);
            -webkit-border-radius: 2px;
            -webkit-box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.1);
            background-color: #fafafa!important;
            border-radius: 2px;
            border: 1px solid #eeeeee;
            box-shadow: none;
            height: 38px;
            color: rgba(0, 0, 0, 0.6)!important;
            font-size: 14px;
        }
        div.panel-login-body {
            padding: 25px 30px;
        }
        h3.mtp-0{
            margin-top: 15px;
        }
        h3.mbt-15{
            margin-bottom: 40px;
        }
        h4.mtp-0{
            margin-top: 0px;
        }
        button.btn {
            border-radius: 0px;
            padding: 6px 14px;
        }
        div.accountbg {
            /*background: url(img/login.jpg);*/
            position: absolute!important;
            height: 100%!important;
            width: 100%!important;
            top: 0!important;
        }
        span.login {
            color: #3292e0!important;
            font-family: 'Crete Round', serif!important;
        }
        a.login-admin {
            font-size: 28px!important;
        }
        a.logo {
            color: #2a323c !important;
            font-size: 20px!important;
            font-weight: 700!important;
            letter-spacing: .03em!important;
            line-height: 70px!important;
        }
        input.form-control:focus {
            background: #ffffff!important;
            border: 1px solid #e0e0e0!important;
            box-shadow: none!important;
        }

        {{--html, body {--}}
            {{--font-family: 'Raleway', sans-serif;--}}
            {{--background: #efefef;--}}
        {{--}--}}
        {{--div.flex-align {--}}
            {{--position: absolute;--}}
            {{--top: 20%;--}}
            {{--width: 100%;--}}
            {{--left: 0;--}}
        {{--}--}}
        {{--.info {--}}
            {{--margin-top: 10%;--}}
            {{--text-align: center;--}}
        {{--}--}}
        {{--.form {--}}
            {{--background: #FFFFFF;--}}
            {{--border-top-left-radius: 3px;--}}
            {{--border-top-right-radius: 3px;--}}
            {{--border-bottom-left-radius: 3px;--}}
            {{--border-bottom-right-radius: 3px;--}}
            {{--padding: 20px;--}}
        {{--}--}}
        {{--.form .preview{--}}
            {{--text-align: center;--}}
        {{--}--}}
        {{--.form img{--}}
            {{--width: 150px;--}}
            {{--height:150px;--}}
            {{--margin-bottom: 5%;--}}
        {{--}--}}

        {{--.form input {--}}
            {{--outline: 0;--}}
            {{--background: #f2f2f2;--}}
            {{--color: #000;--}}
            {{--border: 0;--}}
            {{--border-top-left-radius: 3px;--}}
            {{--border-top-right-radius: 3px;--}}
            {{--border-bottom-left-radius: 3px;--}}
            {{--border-bottom-right-radius: 3px;--}}
            {{--box-sizing: border-box;--}}
            {{--font-size: 18px;--}}
        {{--}--}}
        {{--.btn{--}}
            {{--width: 100%;--}}
        {{--}--}}

        {{--.col-md-offset-4-5{margin-left:37.499996666667%}--}}

        {{--@media (max-width: 768px) {--}}
            {{--.col-md-offset-4-5{margin-left:0%}--}}
        {{--}--}}
    </style>
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- Jquery -->
    <script src=" {{ asset('vendor/jquery/dist/jquery.min.js') }}"></script>
    <!-- Bootstrap -->
    <script src=" {{ asset('vendor/AdminLTE/bootstrap/js/bootstrap.min.js') }}"></script>
</head>
<body>
    @yield('content')
</body>
</html>