<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="#" type="image/x-icon"/>
    <link rel="shortcut icon" href="#" type="image/x-icon"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ config('app.name') }}</title>
    @include('layouts.adminLte.partials.css')
    @yield('css-top')
    @include('layouts.adminLte.partials.js')
</head>
<body class="sidebar-mini sidebar-collapse skin-blue-light">
    <div class="wrapper">
        <!-- Header -->
        @include('layouts.adminLte.partials.header')

        <!-- Sidebar -->
        @include('layouts.adminLte.partials.side')

        <div class="content-wrapper" style="margin-top: 3.80%">
            <section class="content">
                @yield('content')
            </section>
        </div>

        <!-- Footer -->
        @include('layouts.adminLte.partials.footer')
        @yield('script-footer')


    </div>
</body>
</html>