<!-- Bottstrap -->
<link rel="stylesheet" href="{{ asset('vendor/AdminLTE/bootstrap/css/bootstrap.min.css') }}">

<!-- Font Awesome Icons -->
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<!-- Ionicons -->
<link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />
<!-- AdminLte -->
<link rel="stylesheet" href="{{ asset('vendor/AdminLTE/dist/css/AdminLTE.min.css') }}">
<link rel="stylesheet" href="{{ asset('vendor/AdminLTE/dist/css/skins/_all-skins.min.css') }}">
<!-- mapbox -->
<link href='https://api.mapbox.com/mapbox-gl-js/v0.53.0/mapbox-gl.css' rel='stylesheet' />
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('vendor/AdminLTE/plugins/select2/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('vendor/jquery-confirm/css/jquery-confirm.css') }}">
<link rel="stylesheet" href="{{ asset('css/all.css') }}">