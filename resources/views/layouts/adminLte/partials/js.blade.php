<!-- Jquery -->
<script src=" {{ asset('vendor/jquery/dist/jquery.min.js') }}"></script>
<script src=" {{ asset('js/all.js') }}"></script>
<!-- Bootstrap -->
<script src=" {{ asset('vendor/AdminLTE/bootstrap/js/bootstrap.min.js') }}"></script>
<script src=" {{ asset('vendor/AdminLTE/dist/js/app.min.js') }}"></script>
<script src=" {{ asset('vendor/AdminLTE/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
<link rel="stylesheet" href="{{asset('js/plugins/custombox/custombox.min.css')}}">
<script src="{{asset('js/plugins/custombox/custombox.min.js')}}"></script>
<script src="{{asset('js/plugins/custombox/custombox.legacy.min.js')}}"></script>
<!-- Jquery Confirm -->
<script src="{{ asset('vendor/jquery-confirm/js/jquery-confirm.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/js/select2.min.js"></script>
<!-- Input Mask -->
<script src="{{ asset('vendor/AdminLTE/plugins/input-mask/inputmask.dependencyLib.jquery.js') }}"></script>
<!-- Input Mask -->
<script src="{{ asset('vendor/AdminLTE/plugins/input-mask/jquery.inputmask.js') }}"></script>
<script src="{{ asset('vendor/AdminLTE/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
<script src="{{ asset('vendor/AdminLTE/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
<script src="{{ asset('vendor/AdminLTE/plugins/input-mask/jquery.inputmask.numeric.extensions.js') }}"></script>
<script src="{{ asset('vendor/AdminLTE/plugins/input-mask/jquery.inputmask.phone.extensions.js') }}"></script>
<script src="{{ asset('vendor/AdminLTE/plugins/input-mask/jquery.inputmask.regex.extensions.js') }}"></script>
<!-- mapbox -->
<script src='https://api.mapbox.com/mapbox-gl-js/v0.53.0/mapbox-gl.js'></script>
<!-- Select 2 -->
<script src="{{ asset('vendor/AdminLTE/plugins/select2/select2.min.js') }}"></script>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
    var urlBase = '{{ url('') }}/';
    var _token = '{{csrf_token()}}';
</script>
{{--<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBM0AgYoZ8noHQdDepnk77vfCuNGLDLgmw&callback=initMap">--}}
{{--</script>--}}
{{--<script>--}}
      {{--function initMap() {--}}
        {{--var uluru = {lat: -7.1740139, lng: -34.8566748};--}}
        {{--var map = new google.maps.Map(document.getElementById('map'), {--}}
          {{--zoom: 14,--}}
          {{--center: uluru--}}
        {{--});        --}}
        {{--var result;--}}
         {{----}}
         {{--$.get(urlBase + "registrada/mapa", function(response){--}}
            {{--result = response;     --}}
            {{--console.log(result);--}}
            {{--$.each(result, function (index, row) {--}}
              {{--var marker = new google.maps.Marker({--}}
                {{--position: {lat: Number(row.lat), lng: Number(row.lng)},--}}
                {{--map: map--}}
              {{--});--}}
            {{--});--}}
        {{--});--}}
      {{--}--}}
{{--</script>--}}