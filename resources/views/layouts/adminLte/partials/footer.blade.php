<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        Version <b>{{ config('app.version') }}</b>
    </div>
    <!-- Default to the left -->
    <strong>suportehelp@help.com.br © <?php echo date("Y"); ?> Todos os direitos reservados.
</footer>