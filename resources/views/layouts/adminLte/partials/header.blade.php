<header class="main-header" style="position: fixed;">
    <!-- Logo -->
    <a href="{{ url('/') }}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><i class="fa fa-ambulance"></i></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg">Help</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" style="width: 97%; position: fixed;">
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="glyphicon glyphicon-option-vertical"></i>
                    </a>

                    <div class="dropdown-menu dropdown-menu-custom">
                        <a href="{{url('/')}}" class="a-custom-s">
                            <i class="glyphicon glyphicon-user"> Perfil</i>
                        </a>
                        <div class="dropdown-divisor"></div>
                        <a href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                       document.getElementById('logout-form').submit();" class="a-custom-d">
                            <i class="glyphicon glyphicon-off"> Sair</i>
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
{{--    <script>--}}
{{--        $('a').click(function() {--}}
{{--            $(this).find('i').toggleClass('ion-close-round ion-navicon-round');--}}
{{--            $('.sidebar-toggle').toggleClass('active');--}}
{{--        });--}}
{{--    </script>--}}
</header>