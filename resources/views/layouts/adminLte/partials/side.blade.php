<aside class="main-sidebar" style="position: fixed;">
    <section class="sidebar">
        <ul class="sidebar-menu tree" data-widget="tree">
            <li class="header">Acesso Rápido</li>
            <li>
                <a href="{{ url('/') }}">
                    <i class="fa fa-dashboard"></i>
                    <span>Dashboard</span>
                </a>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="ion-arrow-graph-up-right" aria-hidden="true"></i>
                    <span>Ocorrências</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-right ctm pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu" style="display: none; ">
                    <li class="treeview-menu-warning">
                        <a href="{{ url('/solicitacao/pendente') }}">
                            <i class="ion-iphone" style="font-size: 26px;"></i><i class="ion-ios-plus-empty" style="font-size: 15px; margin-left: 2%;"></i>
                            Pendente
                        </a>
                    </li>
                    <li class="treeview-menu-info">
                        <a href="{{ url('/solicitacao/em-atendimento') }}">
                            <i class="ion-iphone" style="font-size: 26px;"></i><i class="ion-ios-close-empty" style="font-size: 15px; margin-left: 2%;"></i>
                            Em Atendimento
                        </a>
                    </li>
                    <li class="treeview-menu-success">
                        <a href="{{ url('/solicitacao/atendida') }}">
                            <i class="ion-iphone" style="font-size: 26px;"></i><i class="ion-ios-checkmark-empty" style="font-size: 15px; margin-left: 2%;"></i>
                            Atendida
                        </a>
                    </li>
                    <li class="treeview-menu-danger">
                        <a href="{{ url('/solicitacao/cancelada') }}">
                            <i class="ion-iphone" style="font-size: 26px;"></i><i class="ion-ios-minus-empty" style="font-size: 15px; margin-left: 2%;"></i>
                            Cancelada
                        </a>
                    </li>
                </ul>
            </li>
            <li class="header">Configurações Sistema</li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-cogs" aria-hidden="true"></i>
                    <span>Configurações</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-right ctm pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu" style="display: none;">
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-table" aria-hidden="true"></i>
                            <span>Tabela Sistema</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-right ctm pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu" style="display: none;">
                            {{--<li><a href="{{ url('/cargo') }}"><i class="fa fa-table"></i>Cadastro dos Cargos</a></li>--}}
                            <li><a href="{{ url('/alergia') }}"><i class="fa fa-table"></i>Alergia</a></li>
                            <li><a href="{{ url('/doenca') }}"><i class="fa fa-table"></i>Doenças</a></li>
                            <li><a href="{{ url('/medicamento') }}"><i class="fa fa-table"></i>Medicamento</a></li>
                            {{--<li><a href="{{ url('/colaborador') }}"><i class="fa fa-table"></i>Cadastro dos Colaboradores</a></li>--}}
                            {{--<li><a href="{{ url('/funcao') }}"><i class="fa fa-table"></i>Cadastro das Funções</a></li>--}}
                            {{--<li><a href="{{ url('/marca') }}"><i class="fa fa-table"></i>Cadastro das Marcas</a></li>--}}
                            {{--<li><a href="{{ url('/modelo') }}"><i class="fa fa-table"></i>Cadastro dos Modelos</a></li>--}}
                            {{--<li><a href="{{ url('/permissao') }}"><i class="fa fa-table"></i>Cadastro das Permissões</a></li>--}}
                            {{--<li><a href="{{ url('/viatura') }}"><i class="fa fa-table"></i>Cadastro de Viaturas</a></li>--}}
                        </ul>
                    </li>
                </ul>
            </li>
        </ul>
    </section>
</aside>