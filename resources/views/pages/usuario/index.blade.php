@extends('layouts.adminLte.default')

@section('css-top')

@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-solid">
                <div class="box-body">
                    <h3 class="text-center">Cadastro de Usuário</h3>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="box box-solid">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="notify"></div>
                        </div>
                    </div>
                    <br/>
                    <div class="row">
                        <div class="col-sm-2">
                            <button type="button" class="ctmzbtn btn-custom-primary btn-outline btn-of" onclick="createUsuario();"><i class="fa fa-plus"></i> Adicionar</button>
                        </div>
                        <div class="col-md-6"></div>
                        <div class="col-sm-4">
                            <div class="input-group">
                                <input type="text" class="form-control"  placeholder="buscar...">
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-info btn-flat"><i class="fa fa-filter"></i></button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th style="width: 8%">#</th>
                                    <th>Nome</th>
                                    <th>Email</th>
                                    <th style="width: 8%; text-align: center;">Ação</th>
                                </tr>
                                </thead>
                                <tbody>
                                {{--@foreach($usuario as $row)--}}
                                    {{--<tr>--}}
                                        {{--<td>{{ $row->cpf }}</td>--}}
                                        {{--<td>{{ $row->email }}</td>--}}
                                        <td style="width: 8%;">
                                            <button class="btn btn-cust-primary btn-xs"  onclick="editUsuario()"><i class="glyphicon glyphicon-pencil" title="Editar"></i></button>
                                            <button class="btn btn-cust-danger btn-xs" onclick="destroyUsuario()"><i class="glyphicon glyphicon-trash" title="Excluir"></i></button>
                                        </td>
                                    </tr>
                                {{--@endforeach--}}
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-9">

                        </div>
                        <div class="col-md-3" style="text-align: right;">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @includeIf('layouts.partials.modal', ['idModal' => 'modal-form-alergia', 'idContent' => 'content-modal-alergia'])
@stop

@section('script-footer')
 <script src=" {{ asset('js/pages/usuario.js') }}"></script>
@stop