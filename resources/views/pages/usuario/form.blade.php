<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true"><i class="fa fa-close" onclick="Custombox.modal.close();"></i></span></button>
    <h4 class="modal-title text-center"> {{ (isset($usuario)) ? 'Editar' : ' Adicionar' }} Usuário</h4>
</div>
@if(isset($usuario))
    {!! Form::model($usuario, ['action' => ('UsuarioController@store'), 'method' => 'post', 'id' => 'form-usuario']) !!}
@else
    {!! Form::open(['action' => ('UsuarioController@store'), 'method' => 'post' , 'id' => 'form-usuario']) !!}
@endif

<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
            <div id="notify-alert"></div>
        </div>
    </div>
    <div class="row">
        {!! Form::hidden('id', null, ['id' => 'id']) !!}
        <div class="col-md-12">
            {!! Form::label('cfp', 'CPF:') !!}
            {!! Form::text('email', null, ['class' => 'form-control', 'id' => 'usuario']) !!}
        </div>
    </div>
</div>

<div class="modal-footer">
    <button type="button" class="ctmzbtn btn-custom-default btn-outline btn-of" style="float: left !important;" onclick="Custombox.modal.close();"> <i class="fa fa-ban" aria-hidden="true"></i> Cancelar</button>
    <button type="submit" class="ctmzbtn btn-custom-success btn-outline btn-of" id="salvar-usuario"> <i class="fa fa-check" aria-hidden="true"></i> Salvar</button>
</div>

{!! Form::close() !!}