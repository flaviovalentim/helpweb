<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true"><i class="fa fa-close"></i></span></button>
    <h4 class="modal-title text-center"> <i class="fa fa-unlock-alt" aria-hidden="true"></i> {{ (isset($permissao)) ? 'Editar' : 'Adicionar' }} Permissão</h4>
</div>
@if(isset($permissao))
    {!! Form::model($permissao, ['action' => ('PermissaoController@store'), 'method' => 'post', 'id' => 'form-permissao']) !!}
@else
    {!! Form::open(['action' => ('PermissaoController@store'), 'method' => 'post' , 'id' => 'form-permissao']) !!}
@endif

<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
            <div id="notify-alert"></div>
        </div>
    </div>
    <div class="row">
        {!! Form::hidden('id', null, ['id' => 'id']) !!}
        <div class="col-md-12">
            {!! Form::label('name', 'Nome:') !!}
            {!! Form::text('name', null, ['class' => 'form-control', 'id' => 'name']) !!}
        </div>
    </div>
    <br />
    <div class="row">
        <div class="col-md-12">
            {!! Form::label('description', 'Descrição:') !!}
            {!! Form::text('description', null, ['class' => 'form-control', 'id' => 'description']) !!}
        </div>
    </div>
</div>

<div class="modal-footer">
    <button type="button" class="ctmzbtn btn-custom-default btn-outline btn-of" style="float: left !important;" id="fechar-permissao"> <i class="fa fa-ban" aria-hidden="true"></i> Cancelar</button>
    <button type="submit" class="ctmzbtn btn-custom-success btn-outline btn-of" id="salvar-permissao"> <i class="fa fa-check" aria-hidden="true"></i> Salvar</button>
</div>

{!! Form::close() !!}