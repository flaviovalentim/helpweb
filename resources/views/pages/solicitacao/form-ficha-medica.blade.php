<div class="modal-header modal-lg">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true"><i class="fa fa-close" onclick="Custombox.modal.close();"></i></span></button>
    <h4 class="modal-title text-center">Ficha Paciente</h4>
</div>

<div class="modal-body-lg body-lg">
    <div class="row">
            <class class="col-md-6">
                <div class="box box-primary">
                    <div class="box-body box-profile">

                        <h3 class="profile-username text-center">Dados Paciente</h3>

                        <ul class="list-group list-group-unbordered">
                            <li class="list-group-item">
                                <b>Data Nacimento:</b> <a class="pull-right">{{isset($solicitacao->usuario->dadosPessoais->data_nascimento) ? date("d/m/Y", strtotime($solicitacao->usuario->dadosPessoais->data_nascimento)) : ''}} <b style="color: #333333">Idade:</b>&nbsp;&nbsp;18 Anos</a>
                            </li>
                            <li class="list-group-item">
                                <b>Nome:</b> <a class="pull-right">{{$solicitacao->usuario->dadosPessoais->nome}}</a>
                            </li>
                            <li class="list-group-item">
                                <b>Contato:</b> <a class="pull-right">{{$solicitacao->usuario->dadosPessoais->telefone}}</a>
                            </li>
                            <li class="list-group-item">
                                <b>Contato para Energências:</b> <a class="pull-right">{{$solicitacao->usuario->dadosPessoais->tel_familiar}}</a>
                            </li>
                            <li class="list-group-item">
                                <b>Alergias:</b> <a class="pull-right">Não possui</a>
                            </li>
                            <li class="list-group-item">
                                <b>Grupo Sanguíneo:</b> <a class="pull-right">{{$solicitacao->usuario->dadosPessoais->tipo_sanguineo}}<b style="color: #333333">Doador(a) de Órgãos:</b>&nbsp;&nbsp;Não</a>
                            </li>
                            <li class="list-group-item">
                                <b>Altura:</b> <a class="pull-right">{{$solicitacao->usuario->dadosPessoais->altura}} cm &nbsp;&nbsp;<b style="color: #333333">Peso:</b>&nbsp;&nbsp;{{$solicitacao->usuario->dadosPessoais->peso}} kg</a>
                            </li>
                        </ul>
                    </div>
                </div >
            </class>
            <div class="col-md-6">
                <div class="box box-primary">
                    <div class="box-body box-profile">
                        <h3 class="profile-username text-center">Endereço da Ocorrência</h3>
                        <ul class="list-group list-group-unbordered">
                            <li class="list-group-item">
                                <b>Logradouro:</b> <a class="pull-right">{{$solicitacao->endSolicitacao->logradouro}}</a>
                            </li>
                            <li class="list-group-item">
                                <b>Número:</b> <a class="pull-right">{{$solicitacao->endSolicitacao->numero}}</a>
                            </li>
                            <li class="list-group-item">
                                <b>Bairro:</b> <a class="pull-right">{{$solicitacao->endSolicitacao->bairro}}</a>
                            </li>
                            <li class="list-group-item">
                                <b>Cep:</b> <a class="pull-right">{{$solicitacao->endSolicitacao->cep}}</a>
                            </li>
                            <li class="list-group-item">
                                <b>Complemento:</b> <a class="pull-right">{{$solicitacao->endSolicitacao->complemento}}</a>
                            </li>
                            <li class="list-group-item">
                                <b>Ponto de Referência:</b> <a class="pull-right">{{$solicitacao->endSolicitacao->ponto_referencia}}</a>
                            </li>
                        </ul>
                    </div>
                </div >
            </div>
    </div>
</div>

<div class="modal-footer">
    <button type="button" class="ctmzbtn btn-custom-default btn-outline btn-of" style="float: left !important;" onclick="Custombox.modal.close();"> <i class="fa fa-close" aria-hidden="true"></i> Fechar</button>
    @if($solicitacao->status->descricao != 'Cancelada' && $solicitacao->status->descricao != 'Atendida')
        <div class="btn-group">
            <button type="button" class="ctmzbtn btn-custom-success btn-outline btn-of dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Status
                <span class="caret"></span>
                <span class="sr-only">Toggle Dropdown</span>
            </button>
            <ul class="dropdown-menu" role="menu">
                @if($solicitacao->status->descricao == 'Pendente')
                    <li><a onclick="alterarStatusSolicitacao({{$solicitacao->id}},'Em Atendimento')">Atender</a></li>
                    <li><a onclick="alterarStatusSolicitacao({{$solicitacao->id}},'Cancelar')">Cancelar</a></li>
                @elseif($solicitacao->status->descricao == 'Em Atendimento')
                    <li><a onclick="alterarStatusSolicitacao({{$solicitacao->id}},'Atendida')">Concluir</a></li>
                    <li><a onclick="alterarStatusSolicitacao({{$solicitacao->id}},'Cancelar')">Cancelar</a></li>
                @endif
            </ul>
        </div>
    @endif
</div>

{!! Form::close() !!}