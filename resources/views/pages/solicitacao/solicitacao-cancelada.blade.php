@extends('layouts.adminLte.default')

@section('css-top')

@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-solid">
                <div class="box-body">
                    <h3 class="text-center">Ocorrências Cancelada</h3>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="box box-solid">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="notify"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th style="width: 20%">Nº Ocorrência</th>
                                    <th>Data Início</th>
                                    <th>Data Término</th>
                                    <th>Status</th>
                                    <th>Ficha Paciente</th>
{{--                                    <th style="width: 8%; text-align: center;">Ação</th>--}}
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($solicitacao as $row)
                                    <tr>
                                        <td>{{$row->num_ocorrencia}}</td>
                                        <td>{{isset($row->data_inicio) ? date("d/m/Y", strtotime($row->data_inicio)) : ''}}</td>
                                        <td>{{isset($row->data_termino) ? date("d/m/Y", strtotime($row->data_termino)): ''}}</td>
                                        <td>{{$row->status->descricao}}</td>
                                        <td style="width: 10%" align="center">
                                            <button class="btn btn-cust-warning btn-x" onclick="VisFichaMedica({{$row->id}})"><i class="glyphicon glyphicon-list-alt" data-toggle="tooltip" title="Visualizar"></i></button>
                                        </td>
{{--                                        <td style="width: 10%" align="center">--}}
{{--                                            <button class="btn btn-cust-primary btn-xs" onclick=""><i class="glyphicon glyphicon-pencil" data-toggle="tooltip" title="Editar"></i></button>--}}
{{--                                            <button class="btn btn-cust-danger btn-xs" onclick=""><i class="glyphicon glyphicon-trash" data-toggle="tooltip" title="Excluir"></i></button>--}}
{{--                                        </td>--}}
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-9">
                            {!! $solicitacao->render(); !!}
                        </div>
                        <div class="col-md-3" style="text-align: right;">
                            <br/>
                            Mostrando {!! $solicitacao->firstItem() !!} a {!! $solicitacao->lastItem() !!} de {!! $solicitacao->total() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @includeIf('layouts.partials.modal', ['idModal' => 'modal-form-ficha-medica', 'idContent' => 'content-modal-solicitacao'])
@stop

@section('script-footer')
    <script src=" {{ asset('js/pages/solicitacao.js') }}"></script>
    <script src=" {{ asset('js/pages/solicitacao-alterar-status.js') }}"></script>
@stop