@extends('layouts.adminLte.default')

@section('css-top')

@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-solid">
                <div class="box-body">
                    <h3 class="text-center">Cadastro de Alergia</h3>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="box box-solid">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="notify"></div>
                        </div>
                    </div>
                    <br/>
                    <div class="row">
                        <div class="col-md-8">
                            <button type="button" class="ctmzbtn btn-custom-primary btn-outline btn-of" onclick="createAlergia();"><i class="fa fa-plus"></i> Adicionar</button>
                        </div>
                        <div id='alert' class="alert " style="display: none;"></div>
                        <div class="row mobile">
                            {!! Form::open(['action' => ('AlergiaController@index'), 'id' => 'form', 'method' => 'GET']) !!}
                            <div class="col-md-4 col-md-offset-left">
                                <div class="input-group">
                                    {!! Form::text('filter', null, ['class' => 'form-control form-control-custom col-md-offset-left margin-input-filter', 'placeholder' => 'Filtrar...']) !!}
                                    <span class="input-group-btn">
                                        <button type="submit" class="btn ctmzbtnfloat btn-custom-float btn-outline btn-of btn_icon_filter marginleft"><span class="glyphicon glyphicon-filter" aria-hidden="true"></span></button>
                                    </span>
                                    <span class="input-group-btn">
                                        <a href="{{url('alergia')}}" class="btn ctmzbtnfloat btn-custom-float btn-outline btn-of btn_icon_filter marginleft"><i class="fa fa-eraser"></i></a>
                                    </span>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>Descrição</th>
                                    <th style="width: 8%; text-align: center;">Ação</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($alergia as $row)
                                    <tr>
                                        <td>{{ $row->descricao }}</td>
                                        <td style="width: 8%;">
                                            <button class="btn btn-cust-primary btn-xs"  onclick="editAlergia({{$row->id}})"><i class="glyphicon glyphicon-pencil" title="Editar"></i></button>
                                            <button class="btn btn-cust-danger btn-xs" onclick="destroyAlergia({{ $row->id }})"><i class="glyphicon glyphicon-trash" title="Excluir"></i></button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-9">
                            {!! $alergia->render(); !!}
                        </div>
                        <div class="col-md-3" style="text-align: right;">
                            <br/>
                            Mostrando {!! $alergia->firstItem() !!} a {!! $alergia->lastItem() !!} de {!! $alergia->total() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @includeIf('layouts.partials.modal', ['idModal' => 'modal-form-alergia', 'idContent' => 'content-modal-alergia'])
@stop

@section('script-footer')
 <script src=" {{ asset('js/pages/alergia.js') }}"></script>
@stop