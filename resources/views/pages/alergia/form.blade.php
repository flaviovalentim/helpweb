<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true"><i class="fa fa-close" onclick="Custombox.modal.close();"></i></span></button>
    <h4 class="modal-title text-center"> {{ (isset($alergia)) ? 'Editar' : ' Adicionar' }} Alergia</h4>
</div>
@if(isset($alergia))
    {!! Form::model($alergia, ['action' => ('AlergiaController@store'), 'method' => 'post', 'id' => 'form-alergia']) !!}
@else
    {!! Form::open(['action' => ('AlergiaController@store'), 'method' => 'post' , 'id' => 'form-alergia']) !!}
@endif

<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
            <div id="notify-alert"></div>
        </div>
    </div>
    <div class="row">
        {!! Form::hidden('id', null, ['id' => 'id']) !!}
        <div class="col-md-12">
            {!! Form::label('descricao', 'Descrição:') !!}
            {!! Form::text('descricao', null, ['class' => 'form-control', 'id' => 'alergia']) !!}
        </div>
    </div>
</div>

<div class="modal-footer">
    <button type="button" class="ctmzbtn btn-custom-default btn-outline btn-of" style="float: left !important;" onclick="Custombox.modal.close();"> <i class="fa fa-ban" aria-hidden="true"></i> Cancelar</button>
    <button type="submit" class="ctmzbtn btn-custom-success btn-outline btn-of" id="salvar-alergia"> <i class="fa fa-check" aria-hidden="true"></i> Salvar</button>
</div>

{!! Form::close() !!}