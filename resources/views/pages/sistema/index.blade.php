@extends('layouts.adminLte.default')

@section('css-top')

@stop

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="box box-solid">
                <div class="box-body">
                    <h3 class="text-center"> <i class="fa fa-table" aria-hidden="true"></i> Entidades de Configurações</h3>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <a href="{{ url('/clinica') }}" class="info-box-link">
                <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="fa fa-hospital-o"></i></span>
                    <div class="info-box-content" style="padding: 5%;">
                        <span class="info-box-text">Clinica</span>
                        <span class="info-box-number">2</span>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <a href="{{ url('/funcao') }}" class="info-box-link">
                <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="fa fa-gavel"></i></span>
                    <div class="info-box-content" style="padding: 5%;">
                        <span class="info-box-text">Funções</span>
                        <span class="info-box-number">2</span>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <a href="{{ url('/permissao') }}" class="info-box-link">
                <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="fa fa-unlock-alt"></i></span>
                    <div class="info-box-content" style="padding: 5%;">
                        <span class="info-box-text">Permissões</span>
                        <span class="info-box-number">2</span>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <a href="{{ url('/usuario') }}" class="info-box-link">
                <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="fa fa-users"></i></span>
                    <div class="info-box-content" style="padding: 5%;">
                        <span class="info-box-text">Usuários</span>
                        <span class="info-box-number">2</span>
                    </div>
                </div>
            </a>
        </div>
    </div>
               

@stop

@section('script-footer')

@stop