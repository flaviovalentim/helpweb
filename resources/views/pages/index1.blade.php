@extends('layouts.adminLte.default')

@section('css-top')

@stop

@section('content')
<style>
       #map {
        height: 400px;
        width: 100%;
       }
    </style>
<div class="row">
        <div class="col-md-12">
            <div class="box box-solid">
                <div class="box-body">
                    <h3 class="text-center"> <i class="ion-arrow-graph-up-right" aria-hidden="true"></i> Acompanhamento das Ocorrências</h3>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <a href="{{ url('/solicitacao/pendente') }}" class="info-box-link-warning">
                <div class="info-box">
                    <span class="info-box-icon" style="background-color:transparent!important;"><i class="ion-iphone" ></i><i class="ion-ios-plus-empty" style="font-size: 30px; margin-left: 5%;"></i></span>
                    <div class="info-box-content" style="padding-top: 5%;">
                        <span class="info-box-text" style="margin-left: -15%;">Pendentes</span>
                        <span class="info-box-number" style="margin-left: 75%; font-size: 36px;"></span>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <a href="{{ url('/solicitacao/em-atendimento') }}" class="info-box-link-info">
                <div class="info-box">
                    <span class="info-box-icon" style="background-color:transparent!important;"><i class="ion-iphone"></i><i class="ion-ios-close-empty" style="font-size: 30px; margin-left: 5%;"></i></span>
                    <div class="info-box-content" style="padding-top: 5%;">
                        <span class="info-box-text" style="margin-left: -15%;">Em Atendimento</span>
                        <span class="info-box-number" style="margin-left: 75%; font-size: 36px;">{{$execucao}}</span>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <a href="{{ url('/solicitacao/atendida') }}" class="info-box-link-success">
                <div class="info-box">
                    <span class="info-box-icon" style="background-color:transparent!important;"><i class="ion-iphone"></i><i class="ion-ios-checkmark-empty" style="font-size: 30px; margin-left: 5%;"></i></span>
                    <div class="info-box-content" style="padding-top: 5%;">
                        <span class="info-box-text" style="margin-left: -15%;">Atendidas</span>
                        <span class="info-box-number" style="margin-left: 75%; font-size: 36px;">{{$concluidas}}</span>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <a href="{{ url('/solicitacao/cancelada') }}" class="info-box-link-danger">
                <div class="info-box">
                    <span class="info-box-icon" style="background-color:transparent!important;"><i class="ion-iphone"></i><i class="ion-ios-minus-empty" style="font-size: 30px; margin-left: 5%;"></i></span>
                    <div class="info-box-content" style="padding-top: 5%;">
                        <span class="info-box-text" style="margin-left: -15%;">Cancelada</span>
                        <span class="info-box-number" style="margin-left: 75%; font-size: 36px;">{{$total}}</span>
                    </div>
                </div>
            </a>
        </div>
    </div>
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title"><i class="ion-ios-location-outline"></i> Mapa das Ocorrência</h3>
            </div>
            <div class="box-body no-padding">
              <div class="row">
                <div class="col-md-9 col-sm-8"  style="width: 100%">
                  <div class="pad">
                      <div id='map' style='width: 100%; height: 600px;'></div>

                  </div>
                </div>
              </div>
            </div>
          </div>
    </div>
</div>

@stop

@section('script-footer')
    <script src="{{ asset('js/pages/map.js') }}"></script>
    <script>
        map.on('load', function () {

            map.addLayer({
                'id': 'population',
                'type': 'circle',
                'source': {
                    type: 'vector',
                    url: 'mapbox://examples.8fgz4egr'
                },
                'source-layer': 'sf210',
                'paint': {
// make circles larger as the user zooms from z12 to z22
                    'circle-radius': {
                        'base': 1.75,
                        'stops': [[12, 2], [22, 180]]
                    },
// color circles by ethnicity, using a match expression
// https://docs.mapbox.com/mapbox-gl-js/style-spec/#expressions-match
                    'circle-color': [
                        'match',
                        ['get', 'ethnicity'],
                        'White', '#fbb03b',
                        'Black', '#223b53',
                        'Hispanic', '#e55e5e',
                        'Asian', '#3bb2d0',
                        /* other */ '#ccc'
                    ]
                }
            });
        });
    </script>

@stop