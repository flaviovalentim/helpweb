@extends('layouts.adminLte.default')

@section('css-top')

@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-solid">
                <div class="box-body">
                    <h3 class="text-center"> <i class="fa fa-car" aria-hidden="true"></i> Viatura</h3>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="box box-solid">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                         <div id="notify"></div>
                        </div>
                    </div>
                    <br/>
                    <div class="row">
                        <div class="col-sm-2">
                            <button type="button" class="ctmzbtn btn-custom-primary btn-outline btn-of" onclick="createViatura();"><i class="fa fa-plus"></i> Adicionar</button>
                        </div>
                        <div class="col-md-6"></div>
                        <div class="col-sm-4">
                            <div class="input-group">
                                <input type="text" class="form-control"  placeholder="buscar...">
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-info btn-flat"><i class="fa fa-filter"></i></button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th style="width: 8%">#</th>
                                        <th>Plava</th>
                                        <th>Modelo</th>
                                        <th>Marca</th>
                                        <th>Observação</th>
                                        <th>Ação</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($result as $row)
                                        <tr>
                                            <td>{{ $row->id }}</td>
                                            <td>{{ $row->placa }}</td>
                                            <td>{{ $row->modelo_id }}</td>
                                            <td>{{ $row->marca_id }}</td>
                                            <td>{{ $row->observacao }}</td>
                                            <td style="width: 10%;">
                                                <button class="btn btn-info btn-flat btn-xs"  onclick="editViatura({{$row->id}})"><i class="fa fa-edit"></i></button>
                                                <button class="btn btn-danger btn-flat btn-xs" onclick="destroyViatura({{ $row->id }}, '{{$row->name}}')"><i class="fa fa-trash"></i></button>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-9">
                            {!! $result->render(); !!}
                        </div>
                        <div class="col-md-3" style="text-align: right;">
                            <br/>
                            @if( count($result) > 10)
                                Mostrando {!! $result->firstItem() !!} a {!! $result->lastItem() !!}
                                de {!! $result->total() !!}
                            @else
                                Mostrando {!! $result->firstItem() !!} de {!! $result->total() !!}
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @includeIf('layouts.partials.modal', ['idModal' => 'modal-form-viatura', 'idContent' => 'content-modal-viatura'])
@stop

@section('script-footer')
    <script src=" {{ asset('js/pages/Viatura.js') }}"></script>
@stop