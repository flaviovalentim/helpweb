<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true"><i class="fa fa-close"></i></span></button>
    <h4 class="modal-title text-center"> <i class="fa fa-car" aria-hidden="true"></i> {{ (isset($viatura)) ? 'Editar' : 'Adicionar' }} viatura</h4>
</div>
@if(isset($viatura))
    {!! Form::model($viatura, ['action' => ('ViaturaController@store'), 'method' => 'post', 'id' => 'form-viatura']) !!}
@else
    {!! Form::open(['action' => ('ViaturaController@store'), 'method' => 'post' , 'id' => 'form-viatura']) !!}
@endif

<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
            <div id="notify-alert"></div>
        </div>
    </div>
    <div class="row">
        {!! Form::hidden('id', null, ['id' => 'id']) !!}
        <div class="col-md-4">
            {!! Form::label('placa', 'Placa:') !!}
            {!! Form::text('placa', null, ['class' => 'form-control', 'id' => 'placa']) !!}
        </div>
        <div class="col-md-4">
            {!! Form::label('modelo_id', 'Modelo:') !!}
            <div class="input-group">
                {!! Form::select('pmodelo_id', $modelo , null, ['class' => 'form-control', 'id' => 'select_modelo']) !!}
                <span class="input-group-btn">
                    <button type="button" class="btn btn-info btn-flat" id="add-modelo"><i class="fa fa-plus"></i></button>
                </span>
            </div>
         </div>   
        <div class="col-md-4">
            {!! Form::label('marca_id', 'Permissão :') !!}
            <div class="input-group">
                {!! Form::select('marca_id', $marca , null, ['class' => 'form-control', 'id' => 'select_marca']) !!}
                <span class="input-group-btn">
                    <button type="button" class="btn btn-info btn-flat" id="add-marca"><i class="fa fa-plus"></i></button>
                </span>
            </div>           
        </div>
        <div class="row">    
            <div class="col-md-12">
                {!! Form::label('observacao', 'Observação:') !!}
                {!! Form::text('observacao', null, ['class' => 'form-control', 'id' => 'observacao']) !!}
            </div>
        </div>    
    </div>
    <br />
</div>

<div class="modal-footer">
    <button type="button" class="ctmzbtn btn-custom-default btn-outline btn-of" style="float: left !important;" id="fechar-viatura"> <i class="fa fa-ban" aria-hidden="true"></i> Cancelar</button>
    <button type="submit" class="ctmzbtn btn-custom-success btn-outline btn-of" id="salvar-viatura"> <i class="fa fa-check" aria-hidden="true"></i> Salvar</button>
</div>

{!! Form::close() !!}