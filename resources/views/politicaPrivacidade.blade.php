<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('app.name')}}</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- Styles -->

    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Raleway', sans-serif;
            font-weight: 100;
            height: 100vh;
            margin: 0;
        }

        .home i{
            color: #000;
            text-decoration:none;
        }

    </style>
</head>
<body>
<section>
    <div style="height: 100%;position: fixed;top: 20%;width: 100%;">
        <div style="text-align: center;">
            <h1 style="font-size: 150px;line-height: 100px;color: #f44336;">
            <h3 style="text-transform: uppercase;line-height: 30px;font-size: 21px;color: #313131;font-family: sans-serif;margin: 10px 0;font-weight: 300;">
                <font style="vertical-align: inherit;">
                    <font style="vertical-align: inherit;">SEÇÃO 1 - O QUE FAREMOS PEDIDO DE PERMISSÃO?</font>
                </font>
            </h3>
                <p class="text-muted m-t-30 m-b-30" style="color: #71777a;line-height: 1.6;margin: 0 0 10px;">
                    <font style="vertical-align: inherit;">
                        <br style="vertical-align: inherit;">
                            PARA QUE VOCÊ POSSO ESTÁ INCLUINDO A FOTO DOS CONTANTO <br>
                            SERÁ NECESSÁRIO A PERMISSÃO PARA ACESSAR A CÂMERA</br>
                            E/OU A GALERIA DO SE SMARTFONE
                    </font>
                </p>
        </div>
</section>
</body>
</html>
