<?php

namespace App\Http\Controllers;

use App\Models\DeshBoard;
use App\Models\Solicitacao;
use Illuminate\Http\Request;

class DeshBoardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){

        $pendente       = Solicitacao::where('status_solicitacao_id',1)->count();
        $emAtendimento  = Solicitacao::where('status_solicitacao_id',2)->count();
        $atendida       = Solicitacao::where('status_solicitacao_id',3)->count();
        $cancelada      = Solicitacao::where('status_solicitacao_id',4)->count();

        return view('pages.deshboard.index', compact('pendente','emAtendimento', 'atendida', 'cancelada'));
    }

    public function sistema(){
        return view('pages.deshboard.sistema.index');
    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\DeshBoard  $deshBorad
     * @return \Illuminate\Http\Response
     */
    public function show(DeshBoard $deshBorad)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\DeshBoard  $deshBorad
     * @return \Illuminate\Http\Response
     */
    public function edit(DeshBoard $deshBorad)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\DeshBoard  $deshBorad
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DeshBoard $deshBorad)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\DeshBoard  $deshBorad
     * @return \Illuminate\Http\Response
     */
    public function destroy(DeshBoard $deshBorad)
    {
        //
    }
    public function getLatLong(){
        return response()->json(IluminacaoPublica::select('lat','lng')->get());
    }
}
