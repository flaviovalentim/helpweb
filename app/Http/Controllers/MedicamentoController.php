<?php

namespace App\Http\Controllers;

use App\Models\Medicamento;
use Illuminate\Http\Request;

class MedicamentoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter = $request->input('filter');
        $medicamento = Medicamento::orderBy('descricao');

        if ($filter)
        {
            $medicamento->where("descricao", "LIKE", "%$filter%");
        }

        $medicamento = $medicamento->paginate(10)->appends('filter', request('filter'));
        return view('pages.medicamento.index', compact('medicamento'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.medicamento.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = $request->input('id');

        $medicamento = Medicamento::find($id);

        if(!$medicamento)
        {
            $medicamento = new Medicamento();
        }

        $medicamento->fill($request->all());

        $validate = validator($request->all(), $medicamento->rules(), $medicamento->mensagens);

        if($validate->fails())
        {
            return response()->json(['success' => false, 'msg' => arrayValidator($validate->errors())]);
        }

        $save = $medicamento->save();

        if($save)
        {
            return response()->json(['success' => true, 'msg' => 'Medicamento salva com sucesso!']);
        }else{
            return response()->json(['success' => true, 'msg' => 'Erro ao salvar Medicamento!']);
        }
    }

    public function edit(Medicamento $medicamento)
    {
        return view('pages.medicamento.form', compact('medicamento'));
    }

    public function destroy(Request $request)
    {
        try
        {
            $id = $request->input('id');

            $delete = Medicamento::where('id', $id)->delete();

            if ($delete)
            {
                return response()->json(['success' => true, 'msg' => 'Medicamento excluida com sucesso.']);
            }
            else
            {
                return response()->json(['success' => false, 'msg' => 'Não foi possível excluir a Medicamento!']);
            }
        }
        catch (Exception $e)
        {
            return response()->json(['success' => false, 'msg' => 'Erro ao excluir Medicamento! '.$e]);
        }
    }
}
