<?php

namespace App\Http\Controllers\Api;

use App\Models\EndSolicitacao;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EndSolicitacaoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function all()
    {
        try{
            $endSolic = EndSolicitacao::get();
            return response()->json($endSolic, 200);
        }catch (\Exception $e){
            return response()->json([], 404);
        }
    }

    public function store(Request $request)
    {
        $result = EndSolicitacao::create([
            'logradouro'        => $request['logradouro'],
            'bairro'            => $request['bairro'],
            'ponto_referencia'  => $request['ponto_referencia'],
            'complemento'       => $request['complemento'],
            'numero'            => $request['numero'],
            'cep'               => $request['cep'],
            'latitude'          => $request['latitude'],
            'longitude'         => $request['longitude'],
        ]);
        return response()->json($result);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $result = \App\Models\EndSolicitacao::findOrFail($id);
        $result->update($request->all());
        return response()->json($result);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
