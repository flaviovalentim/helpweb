<?php

namespace App\Http\Controllers\API\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;


class AuthController extends Controller
{
    /**
     * Login user and create token
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [boolean] remember_me
     * @return [string] access_token
     * @return [string] token_type
     * @return [string] expires_at
     */
    public function login(Request $request)
    {
        // Checa se as credenciais informadas existem na base de dados
        if (!Auth::attempt(['cpf' => request('cpf'), 'password' => request('password')])) {
            return response()->json(['message' => 'Unauthorized'], 401);
        }
        $user = $request->user();

        // Revogar todos os tokens habilitados para o usuário que está se autenticando
        $this->revoke($user->id);

        $tokenResult = $user->createToken($user->name);
        $token = $tokenResult->token;

        // Determina o tempo de expiração do token
        if ($request->remember_me) {
            $token->expires_at = Carbon::now()->addYears(3);
        } else {
            $token->expires_at = Carbon::now()->addHour();
        }

        $token->save();

        return response()->json([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
        ], 200);
    }

    /**
     * Logout user (Revoke the token)
     *
     * @return [string] message
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }

    /**
     * Revoke Tokens
     *
     * @param [int] user_id
     * @return void
     */
    public function revoke($user_id) : void
    {
        \DB::table('oauth_access_tokens')->where('user_id', $user_id)->where('revoked', false)->update(['revoked' => true]);
    }

    /**
     * Valid token api
     *
     * @return \Illuminate\Http\Response
     */
    public function validToken()
    {
        return response()->json(['success' => 'valid'], 201);
    }
}
