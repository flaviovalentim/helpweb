<?php

namespace App\Http\Controllers\Api;

use App\Models\Solicitacao;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SolicitacaoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function all()
    {
        try{
            $Solicit = Solicitacao::get();
            return response()->json($Solicit, 200);
        }catch (\Exception $e){
            return response()->json([], 404);
        }
    }

    public function store(Request $request)
    {
        $result = Solicitacao::create([
            'num_ocorrencia'        => $request['num_ocorrencia'],
            'users_id'              => $request['users_id'],
            'end_solicitacao_id'    => $request['end_solicitacao_id'],
            'status_solicitacao_id' => $request['status_solicitacao_id'],

        ]);
        return response()->json($result);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
