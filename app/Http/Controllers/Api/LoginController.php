<?php

namespace App\Http\Controllers\Api;


use http\Env\Response;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use phpseclib\Crypt\Hash;


class LoginController extends Controller
{
    use AuthenticatesUsers;

    public function login(Request $request)
    {
        try {
            $user_name = $request->input("cpf");
            $password = $request->input("password");

            $user = new User;

            $validate = validation($request->all(), $user->rulesLogin(), $user->massagens_login);

            if ($validate->fails()) {
                $error = validationErrrors($validate);
                return response()->json(["type" => "error", "error" => $error], 406);
            }

            $user = $user->where("user", $user_name)->first();

            if ($user && Hash::check($password, $user->password)) {
                $user_id = $user->id;
                return response()->json(User::find($user_id), 200);
            } else {
                return response()->json([], 400);
            }
        }catch (Exception $e){
            return \response()->json(["error" => $e->getMessage()], 500);
        }

    }
}
