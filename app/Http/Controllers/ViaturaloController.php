<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Viatura;
use App\Models\Marca;
use App\Models\Modelo;

class ViaturaloController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = Role::orderBy('id')->paginate(10);
        return view('pages.viatura.index', compact('result'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $marca = toSelect(Marca::all()->toArray(), 'id', 'descricao');
        $modelo = toSelect(Modelo::all()->toArray(), 'id', 'descricao');

        return view('pages.viatura.form', compact('marca', 'modelo'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         try
        {
        
            $id = $request->input('id');
            
            $role = Role::find($id);
            
            if(!$role)
            {
                $role = new Role();
            }

            $role->fill($request->all());

            $role->name = strtoupper($request->name);
            
            $validate = validator($request->all(), $role->rules(), $role->mensages);
            
            if($validate->fails())
            {
                return response()->json(['success' => false, 'msg' => arrayValidator($validate->errors())]);
            }

            $permissoes = $request->input('permissoes');

            if(count($permissoes) == null || count($permissoes) < 1)
            {
                return response()->json(['success' => false, 'msg' => 'Nenhuma permissão foi adicionado a lista.']);
            }


            $save = $role->save();
            
            if($save) 
            {

                if(isset($permissoes))
                {
                    $permissoes = json_decode($permissoes);

                    \DB::table('permission_role')->where('role_id', $id)
                    ->delete();

                    foreach($permissoes as $permission)
                    {
                        \DB::table('permission_role')->insert(['permission_id' => $permission->id, 'role_id' => $role->id]);
                    }
                }
                return response()->json(['success' => true, 'msg' => 'Função salvo com sucesso!']);
            }else
            {
                return response()->json(['success' => false, 'msg' => 'Erro ao salvar Função!']);
            }
        }
        catch(Exception $e)
        {
            return response()->json(['success' => false, 'msg' => 'Erro ao salvar Função! '.$e]);
        }
    }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $marca = toSelect(Marca::all()->toArray(), 'id', 'descricao');
        $modelo = toSelect(Modelo::all()->toArray(), 'id', 'descricao');

        return view('pages.viatura.form', compact('viatura','marca', 'modelo'));
    }
    

    public function destroy($id)
    {
                try
        {
            $id = $request->input('id');

            \DB::table('permission_role')->where('role_id', $id)->delete();
            
            $delete = Role::where('id', $id)->delete();
            
            if($delete)
            {
                return response()->json(['success' => true, 'msg'=> 'Função excluída com sucesso.']);
            } 
            else 
            {
                return response()->json(['success' => false, 'msg' => 'Erro ao excluir Função.']);
            }   
        } 
        catch(Exception $e)
        {
            return response()->json(['success' => false, 'msg' => 'Erro ao Excluir Função! '.$e]);
        }
    }
    }
}
