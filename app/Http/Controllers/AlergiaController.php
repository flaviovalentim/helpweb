<?php

namespace App\Http\Controllers;

use App\Models\Alergia;
use Illuminate\Http\Request;

class AlergiaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter  = $request->input('filter');
        $alergia = Alergia::orderBy('descricao');

        if ($filter)
        {
            $alergia->where("descricao", "LIKE", "%$filter%");
        }

        $alergia = $alergia->paginate(10)->appends('filter', request('filter'));
        return view('pages.alergia.index', compact('alergia'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.alergia.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = $request->input('id');

        $alergia = Alergia::find($id);

        if(!$alergia)
        {
            $alergia = new Alergia();
        }

        $alergia->fill($request->all());

        $validate = validator($request->all(), $alergia->rules(), $alergia->mensagens);

        if($validate->fails())
        {
            return response()->json(['success' => false, 'msg' => arrayValidator($validate->errors())]);
        }

        $save = $alergia->save();

        if($save)
        {
            return response()->json(['success' => true, 'msg' => 'Alergia salva com sucesso!']);
        }else{
            return response()->json(['success' => true, 'msg' => 'Erro ao salvar Alergia!']);
        }
    }

    public function edit(Alergia $alergia)
    {
        return view('pages.alergia.form', compact('alergia'));
    }

    public function destroy(Request $request)
    {
        try
        {
            $id = $request->input('id');

            $delete = Alergia::where('id', $id)->delete();

            if ($delete)
            {
                return response()->json(['success' => true, 'msg' => 'Alergia excluida com sucesso.']);
            }
            else
            {
                return response()->json(['success' => false, 'msg' => 'Não foi possível excluir a Alergia!']);
            }
        }
        catch (Exception $e)
        {
            return response()->json(['success' => false, 'msg' => 'Erro ao excluir Alergia! '.$e]);
        }
    }
}
