<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UsuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = User::orderBy('id')->paginate(10);;
        return view('pages.usuario.index', compact('result'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.usuario.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = $request->input('id');

        $usuario = User::find($id);

        if(!$usuario)
        {
            $usuario = new User();
        }

        $usuario->fill($request->all());

        $validate = validator($request->all(), $usuario->rules(), $usuario->mensagens);

        if($validate->fails())
        {
            return response()->json(['success' => false, 'msg' => arrayValidator($validate->errors())]);
        }

        $save = $usuario->save();

        if($save)
        {
            return response()->json(['success' => true, 'msg' => 'Usuário salva com sucesso!']);
        }else{
            return response()->json(['success' => true, 'msg' => 'Erro ao salvar Usuário!']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $usuario)
    {
        return view('pages.usuario.form', compact('usuario'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try
        {
            $id = $request->input('id');

            $delete = User::where('id', $id)->delete();

            if ($delete)
            {
                return response()->json(['success' => true, 'msg' => 'Usuário excluida com sucesso.']);
            }
            else
            {
                return response()->json(['success' => false, 'msg' => 'Não foi possível excluir a Usuário!']);
            }
        }
        catch (Exception $e)
        {
            return response()->json(['success' => false, 'msg' => 'Erro ao excluir Usuário! '.$e]);
        }
    }
}
