<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Permission;

class PermissaoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = Permission::orderBy('id')->paginate(10);;
        return view('pages.permissao.index', compact('result'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.permissao.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = $request->input('id');
        
        $permission = Permission::find($id);
        
        if(!$permission)
        {
            $permission = new Permission();
        }

        $permission->fill($request->all());

        $permission->name = strtoupper($request->name);
        
        $validate = validator($request->all(), $permission->rules(), $permission->mensages);
        
        if($validate->fails())
        {
            return response()->json(['success' => false, 'msg' => arrayValidator($validate->errors())]);
        }

        $save = $permission->save();

        if($save)
        {
            return response()->json(['success' => true, 'msg' => 'Permissão salva com sucesso!']);
        }else{
            return response()->json(['success' => true, 'msg' => 'Erro ao salvar permissão!']);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Permission $permissao)
    {
        return view('pages.permissao.form', compact('permissao'));
    }

    public function destroy(Request $request)
    {

        try
        {
            $id = $request->input('id');
            
            $delete = Permission::where('id', $id)->delete();
            
            if($delete)
            {
                return response()->json(['success' => true, 'msg'=> 'Permissão excluída com sucesso.']);
            } 
            else 
            {
                return response()->json(['success' => false, 'msg' => 'Não foi possivel excluir a permissão!']);
            }   
        } 
        catch(Exception $e)
        {
            return response()->json(['success' => false, 'msg' => 'Erro ao Excluir Permissão! '.$e]);
        }
        
    }
}
