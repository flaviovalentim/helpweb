<?php

namespace App\Http\Controllers;

use App\Models\EndSolicitacao;
use App\Models\Solicitacao;
use App\Models\statusSolicitacao;
use Illuminate\Http\Request;

class SolicitacaoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function indexPendente(){
        $status = statusSolicitacao::findByDescription("Pendente");
        $solicitacao = Solicitacao::getByStatus($status)->paginate(10);
        return view('pages.solicitacao.index-pendente', compact('solicitacao'));

    }

    public function indexAtendimento(){
        $status = statusSolicitacao::findByDescription("Em Atendimento");
        $solicitacao = Solicitacao::getByStatus($status)->paginate(10);
        return view('pages.solicitacao.solicitacao-em-atendimento', compact('solicitacao'));

    }

    public function indexCancelada(){
        $status = statusSolicitacao::findByDescription("Cancelada");
        $solicitacao = Solicitacao::getByStatus($status)->paginate(10);
        return view('pages.solicitacao.solicitacao-cancelada', compact('solicitacao'));

    }


    public function indexatendida(){
        $status = statusSolicitacao::findByDescription("Atendida");
        $solicitacao = Solicitacao::getByStatus($status)->paginate(10);
        return view('pages.solicitacao.solicitacao-atendida', compact('solicitacao'));

    }

    public function index(){
        $status = statusSolicitacao::findByDescription('Adendida');
        $solicitacao = Solicitacao::getByStatus($status);
        return view('pages.index', compact('solicitacao'));
    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Solicitacao  $solicitacao
     * @return \Illuminate\Http\Response
     */
    public function show(Solicitacao $solicitacao)
    {
        return view('pages.solicitacao.form-ficha-medica', compact('solicitacao'));
    }

    public function edit(Solicitacao $solicitacao)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Solicitacao  $solicitacao
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Solicitacao $solicitacao)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Solicitacao  $solicitacao
     * @return \Illuminate\Http\Response
     */
    public function destroy(Solicitacao $solicitacao)
    {
        //
    }

    public function alterarStatus(Request $request)
    {
        $solicitacao = Solicitacao::find($request->input('id'));
        $status = statusSolicitacao::findByDescription($request->input("status_descricao"));

        if(!$solicitacao->alterarStatus($status)){
            return response()->json(["success" => false, "Solicitação indisponivel"]);
        }

        if($solicitacao->status_solicitacao_id == 3){
            $solicitacao->data_termino = date("Y-m-d");
    }

        $solicitacao->save();
        return response()->json(["success" => true, "Status Alterado com sucesso"]);
    }

}
