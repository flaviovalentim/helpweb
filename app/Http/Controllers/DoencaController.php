<?php

namespace App\Http\Controllers;

use App\Models\Doenca;
use Illuminate\Http\Request;

class DoencaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter = $request->input('filter');
        $doenca = Doenca::orderBy('descricao');

        if ($filter)
        {
            $doenca->where("descricao", "LIKE", "%$filter%");
        }

        $doenca = $doenca->paginate(10)->appends('filter', request('filter'));
        return view('pages.doenca.index', compact('doenca'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.doenca.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = $request->input('id');

        $doenca = Doenca::find($id);

        if(!$doenca)
        {
            $doenca = new Doenca();
        }

        $doenca->fill($request->all());

        $validate = validator($request->all(), $doenca->rules(), $doenca->mensagens);

        if($validate->fails())
        {
            return response()->json(['success' => false, 'msg' => arrayValidator($validate->errors())]);
        }

        $save = $doenca->save();

        if($save)
        {
            return response()->json(['success' => true, 'msg' => 'Doença salva com sucesso!']);
        }else{
            return response()->json(['success' => true, 'msg' => 'Erro ao salvar Doença!']);
        }
    }

    public function edit(Doenca $doenca)
    {
        return view('pages.doenca.form', compact('doenca'));
    }

    public function destroy(Request $request)
    {
        try
        {
            $id = $request->input('id');

            $delete = Doenca::where('id', $id)->delete();

            if($delete)
            {
                return response()->json(['success' => true, 'msg' => 'Doenca excluído com sucesso.']);
            }
            else
            {
                return response()->json(['success' => false, 'msg' => 'Não foi possível excluir o Doenca!']);
            }
        }
        catch(Exception $e)
        {
            return response()->json(['success' => false, 'msg' => 'Erro ao excluir Doenca! '.$e]);
        }
    }
}
