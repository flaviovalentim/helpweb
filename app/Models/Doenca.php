<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Doenca extends Model
{
    protected $table    = 'doencas';
    protected $fillable = ['descricao'];

    public function rules(){
        return[
            'descricao' => 'required|unique:doencas,descricao'. (($this->id) ? ', '. $this->id : ''),
        ];
    }

    public $mensagens = [
        'descricao.required'    => 'A descrição é obrigatória!',
        'descricao.unique'      => 'Descrição já está cadastrado!'
    ];
}
