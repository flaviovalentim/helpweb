<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    protected function validador(array $data)
    {
        return Validador::make($data, [
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    protected function create(array $data)
    {
        return User::create([
            'email' => $data['email'],
            'passwrod' => bcrypt($data['password']),
        ]);
    }
}
