<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EndSolicitacao extends Model
{
    protected $table    = 'end_solicitacao';
    protected $fillable = ['logradouro', 'bairro', 'ponto_referencia', 'complemento', 'numero', 'cep', 'latitude', 'longitude'];


}
