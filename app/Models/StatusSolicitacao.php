<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class statusSolicitacao extends Model
{
    protected $table    = 'status_solicitacao';
    protected $fillable = ['descricao'];

    public function rules(){
        return[
            'descricao' => 'required|unique:status_solicitacao,descricao'. (($this->id) ? ', '. $this->id : ''),
        ];
    }

    public $mensagens = [
        'descricao.required'    => 'A descrição é obrigatório!',
        'descricao.unique'      => 'Descrição já está cadastrado!'
    ];

    public static function findByDescription($descricao){
        return self::where("descricao",$descricao)->first();
    }

}
