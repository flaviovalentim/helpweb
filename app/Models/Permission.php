<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    protected $table = 'permissions';
    protected $fillable = ['name', 'description'];

    public function roles(){
        return $this->belongsToMany('App\Models\Role');
    }

    public function rules() { 
        return [ 
            'name' => 'required|unique:permissions,name'. (($this->id) ? ', ' . $this->id : ''),
        ];
    }
    
    public $mensages = [
        'name.required' => 'O nome é obrigatório!',
        'name.unique' => 'Esse nome já está cadastrado!'
    ];
}
