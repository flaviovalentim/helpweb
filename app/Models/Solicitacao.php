<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Solicitacao extends Model
{
    protected $table    = 'solicitacao';
    protected $fillable = ['num_ocorrencia', 'users_id', 'end_solicitacao_id', 'status_solicitacao_id'];


    public static function getByStatus($status){
        return self::where('status_solicitacao_id',$status->id);
    }

    public function usuario()
    {
        return $this->belongsTo(User::class, 'users_id', 'id');
    }

//    public function usuarioSolicitacao()
//    {
//        return $this->belongsTo(User::class, 'users_solcitacao_id', 'id');
//    }

    public function endSolicitacao()
    {
        return $this->belongsTo(EndSolicitacao::class, 'end_solicitacao_id', 'id');
    }

    public function status()
    {
        return $this->belongsTo(statusSolicitacao::class, 'status_solicitacao_id', 'id');

    }

    public function alterarStatus($status){
        if(!$this->validarStatus($status)){
            return false;
        }

        $this->status_solicitacao_id = $status->id;

        return true;
    }

    private function validarStatus($status){
        if($this->status->descricao == "Pendente" && $status->descricao == "Em Atendimento"){
            return true;
        }

        if($this->status->descricao == "Em Atendimento" && $status->descricao == "Atendida"){
            return true;
        }

        if($this->status->descricao != "Atendida" && $status->descricao == "Cancelada"){
            return true;
        }
        return false;
    }
}