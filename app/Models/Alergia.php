<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Alergia extends Model
{
    protected $table    = 'alergias';
    protected $fillable = ['descricao'];

    public function rules(){
        return[
            'descricao' => 'required|unique:alergias,descricao'. (($this->id) ? ', '. $this->id : ''),
        ];
    }

    public $mensagens = [
        'descricao.required'    => 'A descrição é obrigatório!',
        'descricao.unique'      => 'Descrição já está cadastrado!'
    ];
}
