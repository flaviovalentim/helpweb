<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Medicamento extends Model
{
    protected $table    = 'medicamentos';
    protected $fillable = ['descricao'];

    public function rules(){
        return[
            'descricao' => 'required|unique:medicamentos,descricao'. (($this->id) ? ', '. $this->id : ''),
        ];
    }

    public $mensagens = [
        'descricao.required'    => 'A descrição é obrigatório!',
        'descricao.unique'      => 'Descrição já está cadastrado!'
    ];
}
