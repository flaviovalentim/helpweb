<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Viatura extends Model
{
 	protected $table = 'viatura';

 	protected $fillable = ['placa', 'modelo_id', 'marca_id', 'observacao'];

 	public function Modelo()
    {
        return $this->belongsToMany('App\Models\Modelo');
    }

     	public function Marca()
    {
        return $this->belongsToMany('App\Models\Marca');
    }


    public function rules() { 
        return [ 
            'placa' 	=> 'required|unique:viatura,placa'. (($this->id) ? ', ' . $this->id : ''),
            'marca_id' 	=> 'required|not_in:0',
            'modelo_id' => 'required|not_in:0',
        ];
    }
    
    public $mensagens = [
        'plava.required' 		=> 	'A placa é obrigatório!',
        'placa.unique' 			=>	'Placa já está cadastrado!',
        'modelo_id.required' 	=>	'Modelo não Informado!',
        'modelo_id.not_in'		=>	'Modelo não Selecionado.',
        'marca_id.required'		=>	'Marca não Informada!',
        'marca_id.not_in'		=>	'Marca não Selecionada',

    ];
}
