<?php

namespace App;


use App\Models\DadosPessoais;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'cpf','email', 'password', 'password_mobile',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'password_mobile',
    ];

    public function roles(){
        return $this->belongsToMany('App\Models\Role');
    }
 
    public function hasPermission(Permission $permission)
    {
        return $this->hasAnyRoles($permission->roles);
    }
 
    public function hasAnyRoles($roles)
    {
        if( is_array($roles) || is_object($roles) )
        {
            foreach ($roles as  $role) 
            {
                return $this->hasAnyRoles($role);
            }
        }
         
        return $this->roles->contains('cpf', $roles);
    }

    public function dadosPessoais()
    {
        return $this->hasOne(DadosPessoais::class, 'users_id', 'id');
    }
}
