<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Laravel\Passport\Passport;
use App\Models\Permission;
use App\User;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        Passport::routes();
//        $this->registerPolicies();
//
//        $permissions = null;
//
//        if (\Schema::hasTable('permissions')) {
//
//            $permissions = Permission::with('roles')->get();
//
//            Gate::before(function (User $user) {
//                if ($user->hasAnyRoles('ADMIN')) {
//                    return true;
//                }
//            });
//        }
//        if ($permissions != null) {
//            foreach ($permissions as $permission) {
//                Gate::define($permission->name, function (User $user) use ($permission) {
//                    return $user->hasPermission($permission);
//                });
//            }
//        }
    }
}
