<?php

function arrayValidator($arr) {
        
    $erros = '<ul>';
            
    foreach ($arr->toArray() as $erro)
    {
        foreach ($erro as $msg)
        {
            $erros .= '<li>' .$msg. '</li>';
        }
    }
        
    $erros .= '</ul>';
        
    return $erros;
}

function toSelect( array $object, $key, $value )
{
    if(count($object) > 0)
    {
        $data = array();
    
        $data[0] = 'Selecione';
        foreach ($object as $row) {
            $data[$row[$key]] = $row[$value];
        }

        return $data;
    }else{
        $data = array();

        $data[0] = 'Selecione';
        
        return $data;
    }
}

function arrayToSelect(array $values, $key, $value) {
        if(count($values) > 0)
        {
            $data = array();
        
            $data[0] = 'Selecione';
            foreach ($values as $row) {
                $data[$row[$key]] = $row[$value];
            }

            return $data;
        }else{
            return [''];
        }
        
    }