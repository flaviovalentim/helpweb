<?php

use Illuminate\Database\Seeder;
use App\Models\Medicamento;
class MedicamentoSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $check = \App\Models\Medicamento::all();

        if (count($check) == 0 ){
            $this->addMedicamento('Acetimentadol');
            $this->addMedicamento('Alfacetimetadol');
            $this->addMedicamento('Alfameprodina');
            $this->addMedicamento('Astansil');
            $this->addMedicamento('Diaxox');
            $this->addMedicamento('Ipsilon');
            $this->addMedicamento('Marevan');
            $this->addMedicamento('Sustrate');
            $this->addMedicamento('Transamin');
            $this->addMedicamento('Cardcor');
            $this->addMedicamento('Metformina');
            $this->addMedicamento('Ultrafer');
            $this->addMedicamento('Gliclazida');
            $this->addMedicamento('Stanglit');
            $this->addMedicamento('Xigdou');
        }
    }

    private function addMedicamento($descricao)
    {
        $medicamento = new Medicamento();
        $medicamento->descricao = $descricao;
        $medicamento->save();
    }
}
