<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $check = \App\User::all();
        if (count($check) == 0){
        $this->addNewUser('administrador@help.com', "admin@123");
        $this->addNewUser('flavio@help.com', "abc@123");
        $this->addNewUser('valentim@help.com', "adm@123");
        }
    }

    private function addNewUser($email, $password)
    {
        $user = new User();
        $user->email = $email;
        $user->password = bcrypt($password);
        $user->save();
    }
}
