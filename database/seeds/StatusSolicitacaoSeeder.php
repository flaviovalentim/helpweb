<?php

use Illuminate\Database\Seeder;
use App\Models\StatusSolicitacao;

class StatusSolicitacaoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $check = \App\Models\statusSolicitacao::all();
        if (count($check) == 0) {

            $this->addStatusSolicitacao('Pendente');
            $this->addStatusSolicitacao('Em Atendimento');
            $this->addStatusSolicitacao('Atendida');
            $this->addStatusSolicitacao('Cancelada');
        }
    }

    private function addStatusSolicitacao($descricao)
    {
        $StatusSolicitacao = new StatusSolicitacao();
        $StatusSolicitacao->descricao = $descricao;
        $StatusSolicitacao->save();
    }
}
