<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(StatusSolicitacaoSeeder::class);
        $this->call(AlergiaSeed::class);
        $this->call(DoencaSeed::class);
        $this->call(MedicamentoSeed::class);
    }

}
