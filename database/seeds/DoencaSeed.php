<?php

use Illuminate\Database\Seeder;
use App\Models\Doenca;

class DoencaSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $check = \App\Models\Doenca::all();

        if (count($check) == 0){
            $this->addDoenca('Coração');
            $this->addDoenca('Diabetes Mellitus Tipo 1');
            $this->addDoenca('Diabetes Mellitus Tipo 2');
            $this->addDoenca('Diabetes Mellitus Gestacional');
            $this->addDoenca('Pré-Diabetes');
            $this->addDoenca('Doença Cardíaca');
            $this->addDoenca('Hipertensão Arterial');

        }
    }

    private function addDoenca($descricao)
    {
        $doenca = new Doenca();
        $doenca->descricao = $descricao;
        $doenca->save();
    }
}
