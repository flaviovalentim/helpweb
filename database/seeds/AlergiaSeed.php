<?php

use Illuminate\Database\Seeder;
use App\Models\Alergia;

class AlergiaSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $check = \App\Models\Alergia::all();

        if (count($check) == 0) {

            $this->addAlergia('Penicilina');
            $this->addAlergia('Eritromicina');
            $this->addAlergia('Amoxicilina');
            $this->addAlergia('Ampicilina ');
            $this->addAlergia('Tetraciclina');
            $this->addAlergia('Carbamazepina');
            $this->addAlergia('Lamotrigina ');
            $this->addAlergia('Fenitoína');
            $this->addAlergia('Insulina de origem animal');
            $this->addAlergia('Contraste de iodo');
            $this->addAlergia('Atracúrio');
            $this->addAlergia('Suxametônio');
            $this->addAlergia('Vecurônio');
            $this->addAlergia('Nevirapina');
            $this->addAlergia(' Abacavir');
        }
    }

    private function addAlergia($descricao)
    {
        $alergia = new Alergia();
        $alergia->descricao = $descricao;
        $alergia->save();
    }
}
