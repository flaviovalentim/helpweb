<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableEndSolicitacao extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('end_solicitacao', function (Blueprint $table) {
            $table->increments('id');
            $table->string('logradouro', 100);
            $table->string('bairro', 100);
            $table->string('ponto_referencia', 100);
            $table->string('complemento', 100);
            $table->integer('numero');
            $table->string('cep', 8);
            $table->string('latitude', 50);
            $table->string('longitude', 50);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('end_solicitacao');
    }
}
