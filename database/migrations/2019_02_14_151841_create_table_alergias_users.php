<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAlergiasUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alergias_users', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('alergias_id');
            $table->unsignedInteger('users_id');
            $table->unique(['alergias_id', 'users_id']);
            $table->foreign('users_id')->references('id')->on('users');
            $table->foreign('alergias_id')->references('id')->on('alergias');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alergias_users');
    }
}
