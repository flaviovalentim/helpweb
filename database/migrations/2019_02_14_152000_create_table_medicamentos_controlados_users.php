<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMedicamentosControladosUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medicamentos_users', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('medicamentos_id');
            $table->unsignedInteger('users_id');
            $table->unique(['medicamentos_id', 'users_id']);
            $table->foreign('users_id')->references('id')->on('users');
            $table->foreign('medicamentos_id')->references('id')->on('medicamentos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medicamentos_controlados_users');
    }
}
