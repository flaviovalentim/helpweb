<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDadosPessoais extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dados_pessoais', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome', 60);
            $table->string('logradouro', 100);
            $table->string('bairro', 100);
            $table->integer('cep');
            $table->string('complemento', 100);
            $table->string('ponto_referencia', 100);
            $table->integer('numero');
            $table->string('telefone', 15);
            $table->string('tel_familiar', 15);
            $table->date('data_nascimento');
            $table->string('num_catao_sus', 30)->nullable();
            $table->string('latitude',50)->nullable();
            $table->string('longitude', 50)->nullable();
            $table->string('tipo_sanguineo', 30)->nullable();
            $table->string('sexo', 30);
            $table->boolean('doador_orgaos');
            $table->double('altura');
            $table->double('peso');
            $table->unsignedInteger('users_id');
            $table->foreign('users_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dados_pessoais');
    }
}
