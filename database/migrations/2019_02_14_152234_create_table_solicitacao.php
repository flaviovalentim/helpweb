<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSolicitacao extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solicitacao', function (Blueprint $table) {
            $table->increments('id');
            $table->string('num_ocorrencia');
            $table->date('data_inicio')->nullable();
            $table->date('data_termino')->nullable();
            $table->unsignedInteger('users_id');
//            $table->unsignedInteger('users_solcitacao_id');
            $table->unsignedInteger('end_solicitacao_id');
            $table->unsignedInteger('status_solicitacao_id');
            $table->foreign('users_id')->references('id')->on('users');
//            $table->foreign('users_solcitacao_id')->references('id')->on('users');
            $table->foreign('end_solicitacao_id')->references('id')->on('end_solicitacao');
            $table->foreign('status_solicitacao_id')->references('id')->on('status_solicitacao');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('solicitacao');
    }
}
