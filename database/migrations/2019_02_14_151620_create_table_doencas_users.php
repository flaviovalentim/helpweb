<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDoencasUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doencas_users', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('doencas_id');
            $table->unsignedInteger('users_id');
            $table->unique(['doencas_id', 'users_id']);
            $table->foreign('users_id')->references('id')->on('users');
            $table->foreign('doencas_id')->references('id')->on('doencas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doencas_users');
    }
}
