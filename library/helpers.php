<?php
    use Illuminate\Pagination\LengthAwarePaginator;
    use Illuminate\Support\Facades\Schema;

    function versao(){
        return '1.0.9';
    }
    
    function arrayToSelect(array $values, $key, $value) {
        if(count($values) > 0)
        {
            $data = array();
        
            $data[0] = 'Selecione';
            foreach ($values as $row) {
                $data[$row[$key]] = $row[$value];
            }

            return $data;
        }else{
            return [''];
        }
        
    }
    
    function objectToSelect(array $values, $key, $value) {
        $data = array();

        $data[0] = 'Selecione';
        foreach ($values as $row) {
            if ($row->$value != '') {
                $data[$row->$key] = $row->$value;
            }
        }

        return $data;
    }
    
    function arrayToValidator($arr) {
        
        $erros = '<ul>';
            
        foreach ($arr->toArray() as $erro)
        {
            foreach ($erro as $msg)
            {
                $erros .= '<li>' .$msg. '</li>';
            }
        }
        
        $erros .= '</ul>';
        
        return $erros;
    }
    
    function queryToArray($arr, $key)
    {
        $array = [];
        foreach ($arr as $row)
        {
            $array[] = $row->$key;
        }
        
        return $array;
    }
    
    function paginate($page, $request, $perPage, $dados)
    {
        //Remove da url &page=numero da pagina para não ficar repetindo na url        
        $paramsUrl = str_replace('&page='.$page, "", $request->fullUrl());
        //Total de registro por pagina
        $offset = ($page * $perPage) - $perPage;

        return new LengthAwarePaginator(array_slice($dados, $offset, $perPage, true), count($dados), $perPage, $page, ['path' => $paramsUrl] );   
    } 
    
    function exceptThumb($imagens)
    {
        $arr = [];
        foreach ($imagens as $imagem)
        {
            if(!stripos($imagem, '-thumb'))
            {
                $arr[] = $imagem;
            }
        }
        return $arr;
    }
    
    function saveImage($prefix, $file, $path)
    {
        $validextensions = ["jpeg", "jpg", "png"];
        $extension = $file->getClientOriginalExtension();
        
        if(in_array( strtolower($extension), $validextensions)){
            $nameHash = hash("md5", uniqid(time()));

            $picture = $prefix.'_' .$nameHash. '.'.$extension;
            $pictureThumb = $prefix.'_' .$nameHash. '-thumb.'.$extension;
            $destinationPath = $path;

            Image::make($file)->resize(300, null, function($constraint){
                $constraint->aspectRatio();
            })->save($destinationPath .'/'. $pictureThumb);                   
            $file->move($destinationPath, $picture);
        }else{
            throw new Exception('Erro extenções permitidas: jpg, jpge e png.');
        }
    }
    
    function deleteImage($request)
    {
        $srcThumb = $request->input('src');
        $srcThumb = str_replace(url('/'), '', $srcThumb);
        $src = str_replace('-thumb', '', $srcThumb);
        
        if(!empty($src)){
            $imagens = File::glob(public_path($src), GLOB_MARK);
            File::delete($imagens);   
            
            $imagensThumb = File::glob(public_path($srcThumb), GLOB_MARK);
            File::delete($imagensThumb);
            return response()->json(['success' => true, 'msg' => 'Imagem deletada da pasta com sucesso.']);
        }else{
            return response()->json(['success' => false, 'msg' => 'Não foi possivel deletar a imagem.']);
        }
    }
    
    function getImagens($path, $id)
    {
        $imagens = File::glob($path .$id. '_*', GLOB_MARK);
        $imagens = exceptThumb($imagens);
        $imagens = str_replace('\\', '/', str_replace(public_path(''), url(''), $imagens)) ;
        return $imagens;
    }
    
    function getImagensThumb($path, $id)
    {
        $imagens = File::glob($path .$id. '_*-thumb*' , GLOB_MARK);
        return $imagens = str_replace(public_path(), url('/'), $imagens);
    }

    function listTableForeignKeys($table)
    {
        $conn = Schema::getConnection()->getDoctrineSchemaManager();

        return array_map(function($key) {
                    return $key->getName();
        }, $conn->listTableForeignKeys($table));
    }
    
    function xmlToArray($xml, $options = array()) {
        $defaults = array(
            'namespaceSeparator' => ':', //you may want this to be something other than a colon
            'attributePrefix' => '@', //to distinguish between attributes and nodes with the same name
            'alwaysArray' => array(), //array of xml tag names which should always become arrays
            'autoArray' => true, //only create arrays for tags which appear more than once
            'textContent' => '$', //key used for the text content of elements
            'autoText' => true, //skip textContent key if node has no attributes or child nodes
            'keySearch' => false, //optional search and replace on tag and attribute names
            'keyReplace' => false       //replace values for above search values (as passed to str_replace())
        );
        $options = array_merge($defaults, $options);
        $namespaces = $xml->getDocNamespaces();
        $namespaces[''] = null; //add base (empty) namespace
        //get attributes from all namespaces
        $attributesArray = array();
        foreach ($namespaces as $prefix => $namespace) {
            foreach ($xml->attributes($namespace) as $attributeName => $attribute) {
                //replace characters in attribute name
                if ($options['keySearch'])
                    $attributeName = str_replace($options['keySearch'], $options['keyReplace'], $attributeName);
                $attributeKey = $options['attributePrefix']
                        . ($prefix ? $prefix . $options['namespaceSeparator'] : '')
                        . $attributeName;
                $attributesArray[$attributeKey] = (string) $attribute;
            }
        }

        //get child nodes from all namespaces
        $tagsArray = array();
        foreach ($namespaces as $prefix => $namespace) {
            foreach ($xml->children($namespace) as $childXml) {
                //recurse into child nodes
                $childArray = xmlToArray($childXml, $options);
                list($childTagName, $childProperties) = each($childArray);

                //replace characters in tag name
                if ($options['keySearch'])
                    $childTagName = str_replace($options['keySearch'], $options['keyReplace'], $childTagName);
                //add namespace prefix, if any
                if ($prefix)
                    $childTagName = $prefix . $options['namespaceSeparator'] . $childTagName;

                if (!isset($tagsArray[$childTagName])) {
                    //only entry with this key
                    //test if tags of this type should always be arrays, no matter the element count
                    $tagsArray[$childTagName] = in_array($childTagName, $options['alwaysArray']) || !$options['autoArray'] ? array($childProperties) : $childProperties;
                } elseif (
                        is_array($tagsArray[$childTagName]) && array_keys($tagsArray[$childTagName]) === range(0, count($tagsArray[$childTagName]) - 1)
                ) {
                    //key already exists and is integer indexed array
                    $tagsArray[$childTagName][] = $childProperties;
                } else {
                    //key exists so convert to integer indexed array with previous value in position 0
                    $tagsArray[$childTagName] = array($tagsArray[$childTagName], $childProperties);
                }
            }
        }

        //get text content of node
        $textContentArray = array();
        $plainText = trim((string) $xml);
        if ($plainText !== '')
            $textContentArray[$options['textContent']] = $plainText;

        //stick it all together
        $propertiesArray = !$options['autoText'] || $attributesArray || $tagsArray || ($plainText === '') ? array_merge($attributesArray, $tagsArray, $textContentArray) : $plainText;

        //return node as array
        return array(
            $xml->getName() => $propertiesArray
        );
    }
// função de formatação de datas para views, $value a data, $hour se a formatação vai exibir hora tambem
    function dateToView($value, $hour = null) {

        if(!isset($value)){
            return '';
        }

        if($hour){

            return date('d/m/Y H:i', strtotime($value));
        }

        return date('d/m/Y', strtotime($value));
    }

    function saveImageBase64($prefix, $file, $path)
    {
        $nameHash = hash("md5", uniqid(time()));

        $picture = $prefix.'_' .$nameHash. '.jpg';
        $pictureThumb = $prefix.'_' .$nameHash. '-thumb.jpg';
        $destinationPath = $path;

        Image::make($file)->save($destinationPath .'/'. $picture);

        Image::make($file)->resize(300, null, function($constraint){
            $constraint->aspectRatio();
        })->save($destinationPath .'/'. $pictureThumb);
    }

function dateToSave($data,$hour = null){
    if(!isset($data)){
        return null;
    }
    $formatDate = str_replace("/", ".", $data);
    if($hour){
        return date('Y-m-d H:i', strtotime($formatDate));
    }

    return date('Y-m-d', strtotime($formatDate));
}

function getSigla(){
    $config = \App\Model\Configuracao::select('sigla')->first();

    if($config){
        return $config->sigla;
    }
    return 'DEF';
}

function getSkin(){
    $config = \App\Model\Configuracao::select('skin')->first();

    if($config){
        return $config->skin;
    }
    return 'skin-blue';
}

function getSkinPattern(){
    $config = \App\Model\Configuracao::select('skin')->first();

    if($config){
        return $config->skin."-pattern";
    }
    return 'skin-blue-pattern';
}

