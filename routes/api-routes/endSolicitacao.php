<?php


Route::group(['prefix' => 'endSolicitacao'], function () {

    Route::get('/', 'Api\EndSolicitacaoController@all');

    Route::post('/store', 'Api\EndSolicitacaoController@store');

    Route::post('/update', 'Api\EndSolicitacaoController@update');

});