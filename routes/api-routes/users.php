<?php

Route::group(['prefix' => 'users'], function () {

    Route::get('/', 'Api\UsersController@all');

    Route::post('/store', 'Api\UsersController@store');

    Route::post('/update', 'Api\UsersController@update');

});