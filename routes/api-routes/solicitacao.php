<?php
Route::group(['prefix' => 'solicitacao'], function () {

    Route::get('/', 'Api\SolicitacaoController@all');

    Route::post('/store', 'Api\SolicitacaoController@store');

    Route::post('/update', 'Api\SolicitacaoController@update');

});