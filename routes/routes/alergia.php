<?php

    Route::group(['prefix' => 'alergia'], function () {

        Route::get('/', 'AlergiaController@index');

        Route::get('/create', 'AlergiaController@create');

        Route::post('/store', 'AlergiaController@store');

        Route::get('/edit/{alergia}', 'AlergiaController@edit');

        Route::post('/destroy', 'AlergiaController@destroy');

    });