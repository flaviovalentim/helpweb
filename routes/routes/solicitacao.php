<?php

    Route::group(['prefix' => 'solicitacao'], function () {

        Route::get('/create', 'SolicitacaoController@create');

        Route::post('/store', 'SolicitacaoController@store');

        Route::post('/change/status', 'SolicitacaoController@alterarStatus');

        Route::get('/edit/{solicitacao}', 'SolicitacaoController@edit');

        Route::post('/destroy', 'SolicitacaoController@destroy');

        Route::get('/show/{solicitacao}', 'SolicitacaoController@show');

        Route::get('/cancelada', 'SolicitacaoController@indexCancelada');

        Route::get('/pendente', 'SolicitacaoController@indexPendente');

        Route::get('/em-atendimento', 'SolicitacaoController@indexAtendimento');

        Route::get('/atendida', 'SolicitacaoController@indexatendida');

    });