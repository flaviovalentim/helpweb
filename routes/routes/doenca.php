<?php 

    Route::group(['prefix' => 'doenca'], function () {

        Route::get('/', 'DoencaController@index');

        Route::get('/create', 'DoencaController@create');

        Route::post('/store', 'DoencaController@store');

        Route::get('/edit/{doenca}', 'DoencaController@edit');

        Route::post('/destroy', 'DoencaController@destroy');

    });
