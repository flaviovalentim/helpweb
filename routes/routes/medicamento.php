<?php

    Route::group(['prefix' => 'medicamento'], function () {

        Route::get('/', 'MedicamentoController@index');

        Route::get('/create', 'MedicamentoController@create');

        Route::post('/store', 'MedicamentoController@store');

        Route::get('/edit/{medicamento}', 'MedicamentoController@edit');

        Route::post('/destroy', 'MedicamentoController@destroy');

    });