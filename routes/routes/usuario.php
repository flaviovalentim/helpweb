<?php

    Route::group(['prefix' => 'usuario'], function () {

        Route::get('/', 'UsuarioController@index');

        Route::get('/create', 'UsuarioController@create');

        Route::post('/store', 'UsuarioController@store');

        Route::get('/edit/{usuario}', 'UsuarioController@edit');

        Route::post('/destroy', 'UsuarioController@destroy');

    });