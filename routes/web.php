<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


Auth::routes();

Route::group(['middleware' => 'auth', 'prefix' => '/'], function () {

    Route::get('/', 'DeshBoardController@index');
    Route::get('/sistema', 'DeshBoardController@index');

    include('routes/alergia.php');
    include('routes/doenca.php');
    include('routes/medicamento.php');
    include('routes/permissao.php');
    include('routes/solicitacao.php');
    include('routes/usuario.php');
});