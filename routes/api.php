<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('login', 'API\Auth\AuthController@login');
Route::post('/create', 'Auth\RegisterController@create');


Route::group(['middleware'=>['auth:api']], function(){
    Route::get('/user', function (Request $request) {
        return $request->user();
    })->middleware('auth:api');



});

include ('api-routes/alergias.php');
include ('api-routes/doencas.php');
include ('api-routes/medicamentos.php');
include ('api-routes/users.php');
include ('api-routes/endSolicitacao.php');
include ('api-routes/solicitacao.php');


Route::get('/politica', function () {
    return view('politicaPrivacidade');
});


