function createAlergia() {
    $.get(urlBase + "alergia/create", function(response){
        $("#content-modal-alergia").html(response);

        var modal = new Custombox.modal({
            content: {
                effect: 'fadein',
                target: '#content-modal-alergia'
            }
        });
        modal.open();
    });
}

function editAlergia(id) {
    $.get(urlBase + "alergia/edit/" + id, function(response){
        $("#content-modal-alergia").html(response);
        var modal = new Custombox.modal({
            content: {
                effect: 'fadein',
                target: '#content-modal-alergia'
            }
        });
        modal.open();
    });
}

function destroyAlergia(id, descricao){
    $.confirm({
        title: 'Excluir Alergia!',
        content: 'Deseja realmente excluir a Alergia <strong>' + descricao + '</strong> ?',
        buttons: {
            removerEntrevista: {
                btnClass: 'btn-red',
                text: 'Sim',
                action: function () {
                    waitingDialog.show('Carregando...', {dialogSize: 'md', progressType: 'success'});
                    $.ajax({
                        type: "POST",
                        url: urlBase + "alergia/destroy",
                        data: {_token: _token, id: id, descricao: descricao},
                        success: function(data){
                            if (data.success == true) {
                                notify('success', data.msg);
                                setTimeout(function(){ location.reload(); }, 1000);
                            }else{
                                notify('warning', data.msg);
                            }

                            waitingDialog.hide();
                        }, error: function(){
                            notify('danger', 'Erro ao excluir.');
                            waitingDialog.hide();
                        }
                    });
                }
            },
            cancel: {
                text: 'Não',
                action: function() {return true;}
            }
        }
    });
}

$(document).ready(function(){

    $('#content-modal-alergia').on('submit', '#form-alergia', function(e){
        e.preventDefault();
        waitingDialog.show('Carregando...', {dialogSize: 'md', progressType: 'success'});
        document.getElementById("salvar-alergia").disabled = true;

        $.ajax({
            type: "POST",
            dataType: 'json',
            url: urlBase + "alergia/store",
            data: $("#form-alergia").serialize(),
            success: function(result){
                if (result.success === true) {
                    notifyAlert('success', result.msg);
                    setTimeout(function(){
                        $('#modal-form-alergia').modal('toggle');
                        location.reload();
                    }, 2000);
                }else{
                    document.getElementById("salvar-alergia").disabled = false;
                    notifyAlert('warning', result.msg);
                }
                waitingDialog.hide();
            }
        });
    });
});