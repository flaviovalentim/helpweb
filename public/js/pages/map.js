mapboxgl.accessToken = 'pk.eyJ1IjoiZmxhdmlvdmFsZW50aW0iLCJhIjoiY2prOHF1NzZkMm43eTNxbnRuZTg1eGt5NSJ9.C11JA2kTQfZGkN-Omkje2w';
var map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/mapbox/light-v9',
    zoom: 12,
    center: [-34.890839, -7.143950]


});

var geojson = {
    type: 'FeatureCollection',
    features: [{
        type: 'Feature',
        geometry: {
            type: 'Point',
            coordinates: [-34.88375804, -7.10897565]
        },
        properties: {
            title: 'Cristo Redentor',
            description: 'Nº Ocorrência: 20190400001'
        }
    },
        {
            type: 'Feature',
            geometry: {
                type: 'Point',
                coordinates: [-34.826047, -7.124874]
            },
            properties: {
                title: 'Cabo Branco',
                description: 'Nº Ocorrência: 20190400001'
            }
        },
        {
            type: 'Feature',
            geometry: {
                type: 'Point',
                coordinates: [-34.859695, -7.105573]
            },
            properties: {
                title: 'Madacaru',
                description: 'Nº Ocorrência: 20190300003'

            }
        }]
};

// add markers to map
geojson.features.forEach(function(marker) {

    // create a HTML element for each feature
    var el = document.createElement('div');
    el.className = 'marker';

    // make a marker for each feature and add to the map
    new mapboxgl.Marker(el)
        .setLngLat(marker.geometry.coordinates)
        .addTo(map);

    new mapboxgl.Marker(el)
        .setLngLat(marker.geometry.coordinates)
        .setPopup(new mapboxgl.Popup({ offset: 25 }) // add popups
            .setHTML('<h3>' + marker.properties.title + '</h3><p>' + marker.properties.description + '</p>'))
        .addTo(map);
});


