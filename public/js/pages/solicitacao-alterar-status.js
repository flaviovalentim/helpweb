function alterarStatusSolicitacao(id, descricao){
    $.confirm({
        title: 'Mudança de Status!',
        content: 'Deseja realmente alterar a solicitação para o status <strong>' + descricao + '</strong> ?',
        buttons: {
            removerEntrevista: {
                btnClass: 'btn-red',
                text: 'Sim',
                action: function () {
                    $.ajax({
                        type: "POST",
                        url: urlBase + "solicitacao/change/status",
                        data: {_token: _token, id: id, status_descricao: descricao},
                        success: function(data){
                            if (data.success == true) {
                                notify('success', data.msg);
                                setTimeout(function(){ location.reload(); }, 1000);
                            }else{
                                notify('warning', data.msg);
                            }
                        }, error: function(){
                            notify('danger', 'Erro ao excluir.');
                        }
                    });
                }
            },
            cancel: {
                text: 'Não',
                action: function() {return true;}
            }
        }
    });
}