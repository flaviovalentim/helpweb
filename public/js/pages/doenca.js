function createDoenca() {
    $.get(urlBase + "doenca/create", function(response){
        $("#content-modal-doenca").html(response);

        var modal = new Custombox.modal({
            content: {
                effect: 'fadein',
                target: '#content-modal-doenca'
            }
        });
        modal.open();
    });
}

function editDoenca(id) {
    $.get(urlBase + "doenca/edit/" + id, function(response){
        $("#content-modal-doenca").html(response);
        var modal = new Custombox.modal({
            content: {
                effect: 'fadein',
                target: '#content-modal-doenca'
            }
        });
        modal.open();
    });
}

function destroyDoenca(id, descricao){
    $.confirm({
        title: 'Excluir Doença!',
        content: 'Deseja realmente excluir o Doença <strong>' + descricao + '</strong> ?',
        buttons: {
            removerEntrevista: {
                btnClass: 'btn-red',
                text: 'Sim',
                action: function () {
                    waitingDialog.show('Carregando...', {dialogSize: 'md', progressType: 'success'});
                    $.ajax({
                        type: "POST",
                        url: urlBase + "doenca/destroy",
                        data: {_token: _token, id: id},
                        success: function(data){
                            if (data.success == true) {
                                notify('success', data.msg);
                                setTimeout(function(){ location.reload(); }, 1000);
                            }else{
                                notify('warning', data.msg);
                            }

                            waitingDialog.hide();
                        }, error: function(){
                            notify('danger', 'Erro ao excluir.');
                            waitingDialog.hide();
                        }
                    });
                }
            },
            cancel: {
                text: 'Não',
                action: function() {return true;}
            }
        }
    });
}

$(document).ready(function(){

    $('#content-modal-doenca').on('submit', '#form-doenca', function(e){
        e.preventDefault();
        waitingDialog.show('Carregando...', {dialogSize: 'md', progressType: 'success'});
        document.getElementById("salvar-doenca").disabled = true;

        $.ajax({
            type: "POST",
            dataType: 'json',
            url: urlBase + "doenca/store",
            data: $("#form-doenca").serialize(),
            success: function(result){
                if (result.success === true) {
                    notifyAlert('success', result.msg);
                    setTimeout(function(){
                        $('#modal-form-doenca').modal('toggle');
                        location.reload();
                    }, 2000);
                }else{
                    document.getElementById("salvar-doenca").disabled = false;
                    notifyAlert('warning', result.msg);
                }
                waitingDialog.hide();
            }
        });
    });
});