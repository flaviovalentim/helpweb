function createUsuario() {
    $.get(urlBase + "usuario/create", function(response){
        $("#content-modal-usuario").html(response);

        var modal = new Custombox.modal({
            content: {
                effect: 'fadein',
                target: '#content-modal-usuario'
            }
        });
        modal.open();
    });
}

function editUsuario(id) {
    $.get(urlBase + "usuario/edit/" + id, function(response){
        $("#content-modal-usuario").html(response);
        var modal = new Custombox.modal({
            content: {
                effect: 'fadein',
                target: '#content-modal-usuario'
            }
        });
        modal.open();
    });
}

function destroyUsuario(id, descricao){
    $.confirm({
        title: 'Excluir Usuário!',
        content: 'Deseja realmente excluir a Usuário <strong>' + descricao + '</strong> ?',
        buttons: {
            removerEntrevista: {
                btnClass: 'btn-red',
                text: 'Sim',
                action: function () {
                    waitingDialog.show('Carregando...', {dialogSize: 'md', progressType: 'success'});
                    $.ajax({
                        type: "POST",
                        url: urlBase + "usuario/destroy",
                        data: {_token: _token, id: id},
                        success: function(data){
                            if (data.success == true) {
                                notify('success', data.msg);
                                setTimeout(function(){ location.reload(); }, 1000);
                            }else{
                                notify('warning', data.msg);
                            }

                            waitingDialog.hide();
                        }, error: function(){
                            notify('danger', 'Erro ao excluir.');
                            waitingDialog.hide();
                        }
                    });
                }
            },
            cancel: {
                text: 'Não',
                action: function() {return true;}
            }
        }
    });
}

$(document).ready(function(){

    $('#content-modal-usuario').on('submit', '#form-usuario', function(e){
        e.preventDefault();
        waitingDialog.show('Carregando...', {dialogSize: 'md', progressType: 'success'});
        document.getElementById("salvar-usuario").disabled = true;

        $.ajax({
            type: "POST",
            dataType: 'json',
            url: urlBase + "usuario/store",
            data: $("#form-usuario").serialize(),
            success: function(result){
                if (result.success === true) {
                    notifyAlert('success', result.msg);
                    setTimeout(function(){
                        $('#modal-form-usuario').modal('toggle');
                        location.reload();
                    }, 2000);
                }else{
                    document.getElementById("salvar-usuario").disabled = false;
                    notifyAlert('warning', result.msg);
                }
                waitingDialog.hide();
            }
        });
    });
});