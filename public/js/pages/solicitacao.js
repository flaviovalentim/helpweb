function VisFichaMedica(id) {
    $.get(urlBase + "solicitacao/show/" + id, function(response){
        $("#content-modal-solicitacao").html(response);
        var modal = new Custombox.modal({
            content: {
                effect: 'fadein',
                target: '#content-modal-solicitacao'
            }
        });
        modal.open();
    });
}