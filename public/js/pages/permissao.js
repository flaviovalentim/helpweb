//functions
function createPermissao(){
    $.get(urlBase + "permissao/create", function(response){
        $("#content-modal-permissao").html(response);
        $('#modal-form-permissao').modal('show');        
    });
}

function editPermissao(id){
    $.get(urlBase + "permissao/edit/" + id, function(response){
        $("#content-modal-permissao").html(response);
        $('#modal-form-permissao').modal('show');
    });
}

function destroyPermissao(id, name){
    $.confirm({
        title: 'Excluir Permissão!',
        content: 'Deseja realmente excluir a Permissão <strong>' + name + '</strong> ?',
        buttons: {
            removerEntrevista: {
                btnClass: 'btn-red',
                text: 'Sim',
                action: function () {
                    waitingDialog.show('Carregando...', {dialogSize: 'md', progressType: 'success'});
                    $.ajax({
                        type: "POST",
                        url: urlBase + "permissao/destroy",
                        data: {_token: _token, id: id},
                        success: function(data){
                            if (data.success == true) {
                                notify('success', data.msg);
                                setTimeout(function(){ location.reload(); }, 1000);
                            }else{
                                notify('warning', data.msg);
                            }
                            
                            waitingDialog.hide();
                        }, error: function(){
                            notify('danger', 'Erro ao excluir.');
                            waitingDialog.hide();
                        }
                    });
                }
            },
            cancel: {
                text: 'Não',
                action: function() {return true;}
            }
        }
    });
}

$(document).ready(function(){
    $('#content-modal-permissao').on('submit', '#form-permissao', function(e){
        e.preventDefault();
        waitingDialog.show('Carregando...', {dialogSize: 'md', progressType: 'success'});
        document.getElementById("salvar-permissao").disabled = true;

        $.ajax({
            type: "POST",
            dataType: 'json',
            url: urlBase + "permissao/store",
            data: $("#form-permissao").serialize(),
            success: function(result){
                if (result.success === true) {
                    notifyAlert('success', result.msg);
                    setTimeout(function(){ 
                        $('#modal-form-permissao').modal('toggle');
                        location.reload();
                    }, 2000);
                }else{
                    document.getElementById("salvar-permissao").disabled = false;
                    notifyAlert('warning', result.msg);
                }
                waitingDialog.hide();
            }
        });
    });
});