function createMedicamento() {
    $.get(urlBase + "medicamento/create", function(response){
        $("#content-modal-medicamento").html(response);

        var modal = new Custombox.modal({
            content: {
                effect: 'fadein',
                target: '#content-modal-medicamento'
            }
        });
        modal.open();
    });
}
function editMedicamento(id) {
    $.get(urlBase + "medicamento/edit/" + id, function(response){
        $("#content-modal-medicamento").html(response);
        var modal = new Custombox.modal({
            content: {
                effect: 'fadein',
                target: '#content-modal-medicamento'
            }
        });
        modal.open();
    });
}
function destroyMedicamento(id, descricao){
    $.confirm({
        title: 'Excluir Medicamento!',
        content: 'Deseja realmente excluir a Medicamento <strong>' + descricao + '</strong> ?',
        buttons: {
            removerEntrevista: {
                btnClass: 'btn-red',
                text: 'Sim',
                action: function () {
                    waitingDialog.show('Carregando...', {dialogSize: 'md', progressType: 'success'});
                    $.ajax({
                        type: "POST",
                        url: urlBase + "medicamento/destroy",
                        data: {_token: _token, id: id},
                        success: function(data){
                            if (data.success == true) {
                                notify('success', data.msg);
                                setTimeout(function(){ location.reload(); }, 1000);
                            }else{
                                notify('warning', data.msg);
                            }

                            waitingDialog.hide();
                        }, error: function(){
                            notify('danger', 'Erro ao excluir.');
                            waitingDialog.hide();
                        }
                    });
                }
            },
            cancel: {
                text: 'Não',
                action: function() {return true;}
            }
        }
    });
}
$(document).ready(function(){

    $('#content-modal-medicamento').on('submit', '#form-medicamento', function(e){
        e.preventDefault();
        waitingDialog.show('Carregando...', {dialogSize: 'md', progressType: 'success'});
        document.getElementById("salvar-medicamento").disabled = true;

        $.ajax({
            type: "POST",
            dataType: 'json',
            url: urlBase + "medicamento/store",
            data: $("#form-medicamento").serialize(),
            success: function(result){
                if (result.success === true) {
                    notifyAlert('success', result.msg);
                    setTimeout(function(){
                        $('#modal-form-medicamento').modal('toggle');
                        location.reload();
                    }, 2000);
                }else{
                    document.getElementById("salvar-medicamento").disabled = false;
                    notifyAlert('warning', result.msg);
                }
                waitingDialog.hide();
            }
        });
    });
});